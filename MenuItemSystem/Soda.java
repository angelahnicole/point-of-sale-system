package MenuItemSystem;

// =====================================================================================================
// Soda.java
// -----------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-426: Advanced Programming
// Date Created: 09/30/13
// Last Modified: 12/01/13
// -----------------------------------------------------------------------------------------------------
// Soda is the child of IceCreamItem. Soda itself defines hasWhippedCream. It can be 
// constructed normally, or by using the methods setData() and parseData(), which uses a comma 
// delimited string to parse and set its attributes.
// =====================================================================================================

public class Soda extends IceCreamItem
{
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//-------------------------------------------------------------------------------------------------
	// DECLARING attributes
	//-------------------------------------------------------------------------------------------------
	
	private boolean hasWhippedCream;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// CONSTRUCTORS
	//--------------------------------------------------------------------------------------------------
	
	public Soda()
	{
		super();
	}
	
	public Soda(String newName, float newCost, float newPrice, String newFlavor, String newIceCream,
			boolean newHasWhippedCream)
	{
		super(newName, newCost, newPrice, newFlavor, newIceCream);
		setHasWhippedCream(newHasWhippedCream);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// METHODS that parse a comma delimited string and sets attributes accordingly.
	//--------------------------------------------------------------------------------------------------
	
	// =================================================================================================
	// PARSEDATA() splits the string into an array, makes sure the right amount of attributes are 
	// present, then uses setDate() to set attributes.
	// =================================================================================================
	protected void parseData(String Data) throws NumberFormatException, IllegalArgumentException
	{
		String[] menuData = Data.split(",");
		
		if(menuData.length == 6)
		{
			super.setData(menuData);
			this.setData(menuData);
		}
		else
		{
			throw new IllegalArgumentException("****ERROR: Input is invalid. Please try again.****");
		}
	}
	
	// =================================================================================================
	// SETDATA() parses and sets members of the string array into the appropriate data type  for each 
	// attribute
	// =================================================================================================
	protected void setData(String[] menuData)
	{
		
		if(menuData[5].equalsIgnoreCase("T") || menuData[5].equalsIgnoreCase("True"))
		{
			setHasWhippedCream(true);
		}
		else if(menuData[5].equalsIgnoreCase("F") || menuData[5].equalsIgnoreCase("False"))
		{
			setHasWhippedCream(false);
		}
		else
		{
			throw new IllegalArgumentException("****ERROR: Input is invalid. Please try again.****");
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// =================================================================================================
	// UNIQUESPECS() prints out the three unique specifications for each object
	// =================================================================================================
	public String uniqueSpecs()
	{
		String newLineMark = System.getProperty("line.separator");
		String leftAlignFormat = " %-7s | %-7s | %-7s |" + newLineMark;
		
		return String.format(leftAlignFormat, getFlavor(), getIceCream(), "" + hasWhippedCream);
	}
	
	// =================================================================================================
	// PRINTTYPE() prints type of menuitem so we don't have to do an instanceof
	// =================================================================================================
	public String printType()
	{
		return "SODA";
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// GETTERS and SETTERS
	//--------------------------------------------------------------------------------------------------

	protected boolean hasWhippedCream()
	{
		return hasWhippedCream;
	}

	protected void setHasWhippedCream(boolean newHasWhippedCream)
	{
		hasWhippedCream = newHasWhippedCream;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
}
