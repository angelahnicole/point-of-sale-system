package MenuItemSystem;

// =====================================================================================================
// MenuExceptionCode.java
// -----------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-426: Advanced Programming
// Date Created: 09/21/13
// Last Modified: 11/30/13
// -----------------------------------------------------------------------------------------------------
// A basic class that returns error messages. Was created for reusability purposes- if I wanted to 
// change one error message, I could change all that use the same message.
// =====================================================================================================

public class MenuExceptionCode
{
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// DECLARING attributes
	//--------------------------------------------------------------------------------------------------
	
	// FILES
	public final int INVALID_FILE_FORMAT = 0;
	public final int FILE_NOT_FOUND = 1;
	public final int FILE_ERROR = 2;
	
	// USER INPUT
	public final int INVALID_INPUT = 3;
	public final int INVALID_NAV_INPUT = 4;
	public final int INVALID_INPUT_INT = 5;
	public final int INVALID_INPUT_STR = 6;
	public final int INVALID_INPUT_FLT = 7;
	public final int INVALID_INPUT_DBL = 8;
	public final int INVALID_INPUT_BOOL = 9;
	
	// MENU ITEMS
	public final int MENU_LIST_EMPTY = 10;
	public final int MENU_ITEM_NOT_FOUND = 11;
	public final int INVALID_TYPE = 12;
	public final int INVALID_COST = 13;
	public final int INVALID_PRICE = 14;
	public final int INVALID_BOOL_COLD = 15;
	public final int INVALID_INT_PREP = 16;
	public final int INVALID_BOOL_SALAD = 17;
	public final int INVALID_INT_COOK = 18;
	public final int INVALID_BOOL_ORGANIC = 19;
	public final int INVALID_BOOL_CAKE = 20;
	public final int INVALID_INT_SERVINGS = 21;
	public final int INVALID_BOOL_SPECIAL = 22;
	public final int INVALID_BOOL_WHIPPED = 23;
	public final int INVALID_INT_SCOOPS = 24;
	public final int INVALID_STRING_COUNTRY = 25;
	public final int INVALID_STRING_FLAVOR = 26;
	public final int INVALID_STRING_ICECREAM = 27;
	
	// ORDERS
	public final int ORDER_NOT_FOUND = 28;
	public final int ORDER_LIST_EMPTY = 29;

	// ORDER STATES
	public final int ORDER_STATE_ALREADY_IN = 30;
	public final int ORDER_STATE_ALREADY_CREATED = 31;
	public final int ORDER_STATE_NEW = 32;
	public final int ORDER_STATE_IN_PROCESS = 33;
	public final int ORDER_STATE_TENDED = 34;
	public final int ORDER_STATE_PAID = 35;
		
	public final int NUM_ERRORS = 36;
	
	private String[] myExceptionMsgs;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// CONSTRUCTOR
	//--------------------------------------------------------------------------------------------------
	
	public MenuExceptionCode() 
	{
		myExceptionMsgs = new String[NUM_ERRORS];
		initMsgs();
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// =================================================================================================
	// INITMSGS() sets error messages according to the constant variable that serves as an index.
	// =================================================================================================
	protected void initMsgs()
	{
		// FILES
		myExceptionMsgs[INVALID_FILE_FORMAT] = 		"****ERROR: File must be in correct format.****";
		myExceptionMsgs[FILE_NOT_FOUND] = 			"****ERROR: File not found.****";
		myExceptionMsgs[FILE_ERROR] = 				"****ERROR: Either an error occurred or you cancelled file opening.****";
		
		// USER INPUT
		myExceptionMsgs[INVALID_INPUT] = 			"****ERROR: Input is invalid. Please check the above format and try again.****";
		myExceptionMsgs[INVALID_NAV_INPUT] = 		"****ERROR: Input is invalid for user navigation. Please enter an integer.****";
		myExceptionMsgs[INVALID_INPUT_INT] = 		"****ERROR: Please enter an integer****";
		myExceptionMsgs[INVALID_INPUT_STR] = 		"****ERROR: Please enter a string.****";
		myExceptionMsgs[INVALID_INPUT_DBL] = 		"****ERROR: Please enter a double (decimal).****";
		myExceptionMsgs[INVALID_INPUT_BOOL] = 		"****ERROR: Please enter a boolean value.****";
		
		// MENU ITEMS
		myExceptionMsgs[MENU_LIST_EMPTY] = 			"****ERROR: Menu list is empty.****";
		myExceptionMsgs[MENU_ITEM_NOT_FOUND] = 		"****ERROR: Menu item not found.****";
		myExceptionMsgs[INVALID_COST] = 			"****ERROR: Cost must be a float, or decimal, in value.****";
		myExceptionMsgs[INVALID_PRICE] = 			"****ERROR: Price must be a float, or decimal, in value.****";
		myExceptionMsgs[INVALID_TYPE] =				"****ERROR: Menu Item type is invalid.****";
		myExceptionMsgs[INVALID_BOOL_COLD] = 		"****ERROR: Is Cold must be a boolean, (i.e. T or F), in value.****";
		myExceptionMsgs[INVALID_INT_PREP] = 		"****ERROR: Prep Time must be an integer in value.****";
		myExceptionMsgs[INVALID_BOOL_SALAD] = 		"****ERROR: Is Salad must be a boolean, (i.e. in value.****";
		myExceptionMsgs[INVALID_INT_COOK] = 		"****ERROR: Is Cook Time must be an integer in value.****";
		myExceptionMsgs[INVALID_BOOL_ORGANIC] = 	"****ERROR: Is Organic must be a boolean, (i.e. T or F), in value.****";
		myExceptionMsgs[INVALID_BOOL_CAKE] = 		"****ERROR: Is Cake must be a boolean, (i.e. T or F), in value.****";
		myExceptionMsgs[INVALID_INT_SERVINGS] = 	"****ERROR: Initial Servings must be an integer in value.****";
		myExceptionMsgs[INVALID_BOOL_SPECIAL] = 	"****ERROR: Is Special must be a boolean, (i.e. T or F), in value.****";;
		myExceptionMsgs[INVALID_BOOL_WHIPPED] = 	"****ERROR: Has Whipped Cream must be a boolean, (i.e. T or F), in value.****";
		myExceptionMsgs[INVALID_INT_SCOOPS] =		"****ERROR: Number of Malt Scoops must be an integer in value.****";
		myExceptionMsgs[INVALID_STRING_COUNTRY] =	"****ERROR: Country of Origin is required.****";
		myExceptionMsgs[INVALID_STRING_FLAVOR] =	"****ERROR: Flavor is required.****";
		myExceptionMsgs[INVALID_STRING_ICECREAM] =	"****ERROR: Ice Cream is required.****";
		
		
		// ORDERS
		myExceptionMsgs[ORDER_LIST_EMPTY] = 		"****ERROR: Order list is empty.****";
		myExceptionMsgs[ORDER_NOT_FOUND] = 			"****ERROR: Order not found.****";
		
		// ORDER STATES
		myExceptionMsgs[ORDER_STATE_ALREADY_IN] =		"****ERROR: That action cannot be completed because it is already in that state.****";
		myExceptionMsgs[ORDER_STATE_ALREADY_CREATED] =	"****ERROR: That action cannot be completed because the order is already created.****";
		myExceptionMsgs[ORDER_STATE_NEW] =				"****ERROR: That action cannot be completed because the order is new. Please add an item first.****";
		myExceptionMsgs[ORDER_STATE_IN_PROCESS] = 		"****ERROR: That action cannot be completed because the order contains items.****";
		myExceptionMsgs[ORDER_STATE_TENDED] = 			"****ERROR: That action cannot be completed because the order contains items.****";
		myExceptionMsgs[ORDER_STATE_PAID] = 			"****ERROR: That action cannot be completed because the order is already completed. Please create another order.****";
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// GETTER for error messages
	//--------------------------------------------------------------------------------------------------
	
	protected String getMsg(int chosenException)
	{	
		return myExceptionMsgs[chosenException];
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
}
