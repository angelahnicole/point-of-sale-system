package MenuItemSystem;

import java.text.DecimalFormat;

// =====================================================================================================
// IceCream.java
//------------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-426: Advanced Programming
// Date Created: 09/30/13
// Last Modified: 12/01/13
// -----------------------------------------------------------------------------------------------------
// IceCream is an adapter class that uses the IceCreamItem hierarchy to create Milkshake and Soda
// objects. 
// =====================================================================================================

public class IceCream extends MenuItem 
{
	////////////////////////////////////////////////////////////////////////////////////////////////////

	//--------------------------------------------------------------------------------------------------
	// DECLARING attributes
	//--------------------------------------------------------------------------------------------------

	// Adapter object
	private IceCreamItem myIceCream;
	
	// Unadapted attribute: Currency. Defaults to USD.
	private Currency myCurrency;
	
	// IceCreamItem factory
	private IceCreamFactory myIceCreamBuilder;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// CONSTRUCTORS
	//--------------------------------------------------------------------------------------------------
	
	public IceCream() 
	{
		super();
		myIceCreamBuilder = new IceCreamFactory();
		setCurrency(new Dollars());
	}
	
	// No need for factory, we already know it's just an IceCream.
	public IceCream(String newName, float newCost, float newPrice, String newFlavor, String newIceCream)
	{
		super(newName, newCost, newPrice);
		myIceCream = new IceCreamItem(newName, newCost, newPrice, newFlavor, newIceCream);
		setCurrency(new Dollars());
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// =================================================================================================
	// READDATA() uses myIceCreamBuilder to create and set the proper attributes for either a Milkshake
	// or a Soda object.
	// =================================================================================================
	protected void parseData(String Data) throws NumberFormatException, IllegalArgumentException
	{	
		myIceCream = myIceCreamBuilder.createIceCream(Data);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// =================================================================================================
	// CALCPROFIT() calculates the profit of selling the menu item.
	// =================================================================================================
	public float calcProfit()
	{
		return myCurrency.ConvertTo(myIceCream.getPrice() - myIceCream.getCost());
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// =================================================================================================
	// CREATECURRENCYFORMAT() returns a DecimalFormat object according to the current Currency object.
	// =================================================================================================
	public DecimalFormat createCurrencyFormat()
	{
		DecimalFormat myCurrencyFormat;
		
		if(myCurrency != null)
		{
			myCurrencyFormat = new DecimalFormat(myCurrency + "###,###.##");
		}
		else
		{
			myCurrencyFormat = new DecimalFormat("$###,###.##");
			System.out.println("ERROR: Currency is null, but has defaulted to USD. These conversions may be incorrect.");
			System.out.println();
		}
		
		myCurrencyFormat.setMaximumFractionDigits(2);
		myCurrencyFormat.setMinimumFractionDigits(2);
		
		return myCurrencyFormat;
	}
	
	// =================================================================================================
	// TOSTRING() prints out object as a formatted string, and only prints out parent's information
	// =================================================================================================
	public String toString()
	{
		DecimalFormat myCurrencyFormat = createCurrencyFormat();
		String leftAlignFormat = "| %-20s | %-7s | %-7s | %-7s |";

		return String.format(leftAlignFormat, getName(), myCurrencyFormat.format(getCost()), 
							myCurrencyFormat.format(getPrice()), myCurrencyFormat.format(calcProfit()));
	}
	
	// =================================================================================================
	// UNIQUESPECS() prints out the three unique specifications for each object
	// =================================================================================================
	public String uniqueSpecs()
	{
		return myIceCream.uniqueSpecs();
	}
	
	// =================================================================================================
	// PRINTTYPE() prints type of menuitem so we don't have to do an instanceof
	// =================================================================================================
	public String printType()
	{
		return myIceCream.printType();
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// GETTERS and SETTERS for Name, Cost, Price, Flavor, and IceCream.
	//--------------------------------------------------------------------------------------------------
	
	protected String getName()
	{
		return myIceCream.getName();
	}
	
	protected float getCost()
	{
		return myCurrency.ConvertTo(myIceCream.getCost());
	}
	
	protected float getPrice()
	{
		return myCurrency.ConvertTo(myIceCream.getPrice());
	}
	
	protected String getFlavor()
	{
		return myIceCream.getFlavor();
	}
	
	protected String getIceCream()
	{
		return myIceCream.getIceCream();
	}
	
	protected Currency getCurrency()
	{
		return myCurrency;
	}
	
	protected void setName(String newName)
	{
		myIceCream.setName(newName);
	}
	
	protected void setCost(float newCost)
	{
		myIceCream.setCost(newCost);
	}
	
	protected void setPrice(float newPrice)
	{
		myIceCream.setCost(newPrice);
	}
	
	protected void setFlavor(String newFlavor)
	{
		myIceCream.setFlavor(newFlavor);
	}
	
	protected void setIceCream(String newIceCream)
	{
		myIceCream.setIceCream(newIceCream);
	}
	
	protected void setCurrency(Currency newCurrency)
	{
		myCurrency = newCurrency;
	}
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
}
