package MenuItemSystem;

import java.text.DecimalFormat;

// =====================================================================================================
// MenuItem.java
//------------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-426: Advanced Programming
// Date Created: 09/12/13
// Last Modified: 12/01/13
// -----------------------------------------------------------------------------------------------------
// MenuItem is a basic class that defines a Name, Cost, Price, and Currency. Mostly because this system
// uses adapted classes (and file reading does not include currency), only this parent and the adapted
// child, IceCream, deals directly with Currency objects. Currency defaults to USD. It can be  
// constructed normally, or by using the methods setData() and parseData(), which uses a comma delimited
//  string to parse and set its attributes.
// =====================================================================================================

public class MenuItem 
{
	////////////////////////////////////////////////////////////////////////////////////////////////////

	//--------------------------------------------------------------------------------------------------
	// DECLARING attributes
	//--------------------------------------------------------------------------------------------------

	private String Name;
	private float Cost;
	private float Price;
	// Defaults to USD- only parent and adapted children need to keep track
	private Currency myCurrency;
	
	protected MenuExceptionCode myMenuException;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// CONSTRUCTORS
	//--------------------------------------------------------------------------------------------------
	
	public MenuItem() 
	{
		myMenuException = new MenuExceptionCode();
		setCurrency(new Dollars());
	}
	
	public MenuItem(String newName, float newCost, float newPrice)
	{
		setName(newName);
		setCost(newCost);
		setPrice(newPrice);
		setCurrency(new Dollars());
		
		myMenuException = new MenuExceptionCode();
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// METHODS that parse a comma delimited string and sets attributes accordingly.
	//--------------------------------------------------------------------------------------------------
	
	// =================================================================================================
	// PARSEDATA() splits the string into an array, makes sure the right amount of attributes are 
	// present, then uses setData() to set attributes.
	// =================================================================================================
	protected void parseData(String Data) throws NumberFormatException, IllegalArgumentException
	{	
		String[] menuData = Data.split(",");
		
		if(menuData.length == 3)
		{
			setData(menuData);
		}
		else
		{
			throw new IllegalArgumentException(myMenuException.getMsg(myMenuException.INVALID_FILE_FORMAT)); 
		} 
	}
	
	// =================================================================================================
	// SETDATA() parses and sets members of the string array into the appropriate data type for each 
	// attribute
	// =================================================================================================
	protected void setData(String[] menuData) throws NumberFormatException, IllegalArgumentException
	{
		if(menuData[0].length() <= 20)
		{
			setName(menuData[0]);
			setCost(Float.parseFloat(menuData[1]));
			setPrice(Float.parseFloat(menuData[2]));
		}
		else
		{
			throw new IllegalArgumentException(myMenuException.getMsg(myMenuException.INVALID_INPUT));
		}
	}
	
	// =================================================================================================
	// SETDESSERTDATA() is like the above, but checks for another string length.
	// =================================================================================================
	protected void setDessertData(String[] menuData) throws NumberFormatException, IllegalArgumentException
	{
		if(menuData[0].length() <= 30)
		{
			setName(menuData[0]);
			setCost(Float.parseFloat(menuData[1]));
			setPrice(Float.parseFloat(menuData[2]));
		}
		else
		{
			throw new IllegalArgumentException(myMenuException.getMsg(myMenuException.INVALID_INPUT));
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// =================================================================================================
	// CALCPROFIT() calculates the profit of selling the menu item.
	// =================================================================================================
	public float calcProfit()
	{
		return myCurrency.ConvertTo(Price - Cost);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// =================================================================================================
	// CREATECURRENCYFORMAT() returns a DecimalFormat object according to the current Currency object.
	// =================================================================================================
	public DecimalFormat createCurrencyFormat()
	{
		DecimalFormat myCurrencyFormat;
		
		if(myCurrency != null)
		{
			myCurrencyFormat = new DecimalFormat(myCurrency + "###,###.##");
		}
		else
		{
			myCurrencyFormat = new DecimalFormat("$###,###.##");
			System.out.println("ERROR: Currency is null, but has defaulted to USD. These conversions may be incorrect.");
			System.out.println();
		}
		
		myCurrencyFormat.setMaximumFractionDigits(2);
		myCurrencyFormat.setMinimumFractionDigits(2);
		
		return myCurrencyFormat;
	}
	
	// =================================================================================================
	// TOSTRING() prints out object as a formatted string, and only prints out parent's information
	// =================================================================================================
	public String toString()
	{
		DecimalFormat myCurrencyFormat = createCurrencyFormat();
		String leftAlignFormat = "| %-20s | %-7s | %-7s | %-7s |";
		
		return String.format(leftAlignFormat, Name, myCurrencyFormat.format(getCost()), 
							myCurrencyFormat.format(getPrice()), myCurrencyFormat.format(calcProfit()));
	}
	
	// =================================================================================================
	// UNIQUESPECS() prints out the three unique specifications for each object
	// =================================================================================================
	public String uniqueSpecs()
	{
		return "";
	}
	
	// =================================================================================================
	// PRINTTYPE() prints type of menuitem so we don't have to do an instanceof
	// =================================================================================================
	public String printType()
	{
		return "MENUITEM";
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// GETTERS and SETTERS for Name, Cost, Price, Name, and Currency.
	//--------------------------------------------------------------------------------------------------
	
	protected String getName()
	{
		return Name;
	}
	
	protected float getCost()
	{
		return myCurrency.ConvertTo(Cost);
	}
	
	protected float getPrice()
	{
		return myCurrency.ConvertTo(Price);
	}
	
	protected Currency getCurrency()
	{
		return myCurrency;
	}
	
	protected void setName(String newName)
	{
		Name = newName;
	}
	
	protected void setCost(float newCost)
	{
		Cost = newCost;
	}
	
	protected void setPrice(float newPrice)
	{
		Price = newPrice;
	}
	
	protected void setCurrency(Currency newCurrency)
	{
		myCurrency = newCurrency;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
}
