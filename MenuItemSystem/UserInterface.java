package MenuItemSystem;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.regex.Pattern;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

// =====================================================================================================
// UserInterface.java
// -----------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-426: Advanced Programming
// Date Created: 09/12/13
// Last Modified: 12/01/13
// -----------------------------------------------------------------------------------------------------
// Obviously, UserInterface works with the user to Add, Remove, Find, Edit, and Display menu items,
// along with creating orders and adding items to it. It first requires the User to choose a currency,
// choose how they would like to sort the menu items (Name, Cost, Price, Profit), and then it wants
// the user to load a file. It also uses AppLogic as a buffer between the UI and the data subsystems 
// and file I/O for encapsulation purposes. 
// =====================================================================================================

public class UserInterface 
{
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// Global variables
    static boolean isEditing = false;
    static boolean fileLoaded = false;
    static MenuExceptionCode myMenuException = new MenuExceptionCode();
    
	////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void main(String[] args) 
    {
		////////////////////////////////////////////////////////////////////////////////////////////////
    	
        // App Logic
        final AppLogic myAppLogic = new AppLogic();
    	final ComparatorFactory myComparatorFactory = new ComparatorFactory();
    	final CurrencyFactory myCurrencyFactory = new CurrencyFactory();
    	
    	// ---------------------------------------------------------------------------------------------
    	// DECLARING/INITIALIZING GUI COMPONENTS
    	// ---------------------------------------------------------------------------------------------
        
    	// Size of components
        Dimension menuItemSize = new Dimension(150, 20);
        Dimension loadFileSize = new Dimension(150, 20);
        Dimension buttonSize = new Dimension(150, 27);
        
        Dimension clearSearchButtonSize = new Dimension(236, 20);
        Dimension searchButtonSize = new Dimension(130, 20);
        Dimension searchTextFieldSize = new Dimension(265, 20);
        
        Dimension comboBoxSize = new Dimension(100, 20);
        Dimension textFieldSize = new Dimension(100, 20);
        
        Dimension menuSize = new Dimension(400, 149);
        Dimension labelSize = new Dimension(105, 20);
        Dimension menuLabelSize = new Dimension(400, 20);
        Dimension confirmSize = new Dimension(713, 20);
        Dimension separatorSize = new Dimension(553, 50);
        
        Dimension orderSize = new Dimension(400, 149);
        
    	// Frames
        final JFrame mainFrame = new JFrame("Menu Item System");
        final JFrame addMenuItemFrame = new JFrame("New Item/Edit Item");
        final JFrame viewOrderFrame = new JFrame("View Order");
        
        // Insets
        Insets mainInsets = mainFrame.getInsets();
        Insets addMenuItemInsets = addMenuItemFrame.getInsets();
        Insets viewOrderInsets = viewOrderFrame.getInsets();
        
        // Main Frame size
        int WIDTH = mainInsets.left + mainInsets.right + 583;
        int HEIGHT = mainInsets.top + mainInsets.bottom + 450;
        
        // File uploading components 
        final JFileChooser fileChooser = new JFileChooser();
        final FileNameExtensionFilter myFilter = new FileNameExtensionFilter("Text Files(.txt)", "txt");
        // Current directory
        fileChooser.setCurrentDirectory(new File("."));
        
        // Menu Item buttons
        final JButton loadFile = new JButton("Load File");
        final JButton addMenuItem = new JButton("Add Menu Item");
        final JButton deleteMenuItem = new JButton("Delete Menu Item");
        final JButton editMenuItem = new JButton("Edit Menu Item");
        final JButton viewMenuItem = new JButton("View Menu Item");
        final JButton clearSearch = new JButton("Clear Search Results");
        
        // Order buttons
        final JButton createOrder = new JButton("Create Order");
        final JButton addItemOrder = new JButton("Add Item To Order");
        final JButton tendOrder = new JButton("Tend Order");
        final JButton payOrder = new JButton("Pay Order");
        final JButton viewOrder = new JButton("View Order");

        // Main Search button and text field
        final JButton Search = new JButton("Search by Name");
        final JTextField searchField = new JTextField();
        
        // Editing Menu Item text fields
        final JTextField Name = new JTextField();
        final JTextField Cost = new JTextField();
        final JTextField Price = new JTextField();
        final JTextField specFieldOne = new JTextField();
        final JTextField specFieldTwo = new JTextField();
        final JTextField specFieldThree = new JTextField();

        // Adding/Editing Menu Item labels
        final JLabel typeLabel = new JLabel("Type(Select One):");
        final JLabel nameLabel = new JLabel("Name:");
        final JLabel costLabel = new JLabel("Cost: " + myAppLogic.getCurrency());
        final JLabel priceLabel = new JLabel("Price: "  + myAppLogic.getCurrency());
        final JLabel specOneLabel = new JLabel("Unique Field 1:");
        final JLabel specTwoLabel = new JLabel("Unique Field 2:");
        final JLabel specThreeLabel = new JLabel("Unique Field 3:");
        
        // Adding/Editing Button
        final JButton addConfirm = new JButton("Submit");
        
        // List Font
        final Font listFont = new Font("Monospaced", Font.PLAIN, 12);
        
        // Menu Item Label
		final String newLineMark = System.getProperty("line.separator");
		final String menuAlignFormat = "| %-20s | %-7s | %-7s | %-7s |" + newLineMark;
        final JLabel menuLabel = new JLabel(String.format(menuAlignFormat, "Menu Item Name", "Cost", "Price", "Profit"));
        menuLabel.setFont(listFont);
        
        // Menu Item List
        final DefaultListModel myMenuListModel = new DefaultListModel();
        final JList myMenuList = new JList(myMenuListModel);
        myMenuList.setFont(listFont);
        final JScrollPane scrollPaneMenu = new JScrollPane();
		scrollPaneMenu.getViewport().add(myMenuList);
		
        // Order Label
		final String orderAlignFormat = "| %-14s | %-10s | %-10s |" + newLineMark;
        final JLabel orderLabel = new JLabel(String.format(orderAlignFormat, "Order No.", "State", "Total"));
        orderLabel.setFont(listFont);
		
		// Order List
		final DefaultListModel myOrderListModel = new DefaultListModel();
        final JList myOrderList = new JList(myOrderListModel);
        myOrderList.setFont(listFont);
        JScrollPane scrollPaneOrder = new JScrollPane();
		scrollPaneOrder.getViewport().add(myOrderList);
		
		// Order Menu Item Label
        final JLabel orderMenuLabel = new JLabel(String.format(menuAlignFormat, "Menu Item Name", "Price", "No.", "Sub."));
        orderMenuLabel.setFont(listFont);
        
        // View Order labels
        final JLabel orderStateLabel = new JLabel("");
        final JLabel orderTotalLabel = new JLabel("");
		
		// Order Menu Item List
		final DefaultListModel myOrderMenuListModel = new DefaultListModel();
        final JList myOrderMenuList = new JList(myOrderMenuListModel);
        myOrderMenuList.setFont(listFont);
        JScrollPane scrollPaneOrderMenu = new JScrollPane();
		scrollPaneOrderMenu.getViewport().add(myOrderMenuList);
		
		// Horizontal separator
		JSeparator horizontalSeparator = new JSeparator();
      
        // Various combo boxes for GUI
        final JComboBox addTypes = new JComboBox(new String[]{"APPETIZER","MEAL","DESSERT","SODA","MILKSHAKE"});
        final JComboBox keyTypes = new JComboBox(new String[]{"NAME","PRICE","COST","PROFIT"});
        final JComboBox currencyTypes = new JComboBox(new String[]{"DOLLAR","YEN","EURO"});
        
        // Different regular expressions to check for valid float and integer types
        final String floatRegex = "^(\\d*\\.?\\d*)$";
        final String intRegex = "^(\\d*)$";
        
		////////////////////////////////////////////////////////////////////////////////////////////////
        
    	// ---------------------------------------------------------------------------------------------
    	// ADDING and setting up main frame GUI components
    	// ---------------------------------------------------------------------------------------------
        
        // Set FileChooser to grab .txt files
        fileChooser.setFileFilter(myFilter);
        
        // Setting Size, Layout, and Resizable options
        mainFrame.setLayout(null);
        mainFrame.setSize(WIDTH, HEIGHT);
        mainFrame.setDefaultCloseOperation(3);
        mainFrame.setResizable(true);
        
		// -----------------------------------------------------------------------------------------------
        
        // SIDE BUTTONS FOR MENU ITEMS
        
        // Loading file - adding to frame, setting size, and setting bounds
        mainFrame.add(loadFile);
        loadFile.setPreferredSize(loadFileSize);
        loadFile.setBounds(mainInsets.left + 5, mainInsets.top + 45, buttonSize.width, buttonSize.height);
        loadFile.setEnabled(false);
        
        // Adding menu item - adding to frame, setting size, and setting bounds
        mainFrame.add(addMenuItem);
        addMenuItem.setPreferredSize(menuItemSize);
        addMenuItem.setBounds(mainInsets.left + 5, mainInsets.top + 75, buttonSize.width, buttonSize.height);
        addMenuItem.setEnabled(false);
        
        // Deleting menu item - adding to frame, setting size, and setting bounds
        mainFrame.add(deleteMenuItem);
        deleteMenuItem.setPreferredSize(menuItemSize);
        deleteMenuItem.setBounds(mainInsets.left + 5, mainInsets.top + 105, buttonSize.width, buttonSize.height);
        deleteMenuItem.setEnabled(false);
        
        // Editing menu Item - adding to frame, setting size, and setting bounds
        mainFrame.add(editMenuItem);
        editMenuItem.setPreferredSize(menuItemSize);
        editMenuItem.setBounds(mainInsets.left + 5, mainInsets.top + 135, buttonSize.width, buttonSize.height);
        editMenuItem.setEnabled(false);
        
        // Viewing menu Item - adding to frame, setting size, and setting bounds
        mainFrame.add(viewMenuItem);
        viewMenuItem.setPreferredSize(menuItemSize);
        viewMenuItem.setBounds(mainInsets.left + 5, mainInsets.top + 165, buttonSize.width, buttonSize.height);
        viewMenuItem.setEnabled(false);
        
		// -----------------------------------------------------------------------------------------------
        
        // MENU ITEMS TOP
        
        // Currency Type Combobox - adding to frame, setting size, and setting bounds
        mainFrame.add(currencyTypes);
        currencyTypes.setPreferredSize(comboBoxSize);
        currencyTypes.setBounds(mainInsets.left + 5, mainInsets.top + 5, menuItemSize.width, menuItemSize.height); 
        currencyTypes.setEnabled(true);
        
        // Filter Type Combobox - adding to frame, setting size, and setting bounds
        mainFrame.add(keyTypes);
        keyTypes.setPreferredSize(comboBoxSize);
        keyTypes.setBounds(mainInsets.left + 160, mainInsets.top + 5, menuItemSize.width, menuItemSize.height);
        keyTypes.setEnabled(false);
        
        // Clear Search button - adding to frame, setting size, and setting bounds
        mainFrame.add(clearSearch);
        clearSearch.setPreferredSize(buttonSize);
        clearSearch.setBounds(mainInsets.left + 324, mainInsets.top + 5, clearSearchButtonSize.width, clearSearchButtonSize.height);
        clearSearch.setEnabled(false);
        
        // -----------------------------------------------------------------------------------------------
        
        // MENU ITEMS BOTTOM
        
        // Search Type Combobox - adding to frame, setting size, and setting bounds
        mainFrame.add(searchField);
        searchField.setPreferredSize(comboBoxSize);
        searchField.setBounds(mainInsets.left + 160, mainInsets.top + 195, searchTextFieldSize.width, searchTextFieldSize.height);
        searchField.setEnabled(false);
        
        // Search button - adding to frame, setting size, and setting bounds
        mainFrame.add(Search);
        Search.setPreferredSize(buttonSize);
        Search.setBounds(mainInsets.left + 429, mainInsets.top + 195, searchButtonSize.width, searchButtonSize.height);
        Search.setEnabled(false);
        
		// -----------------------------------------------------------------------------------------------
        
        // MENU ITEM LIST
        
        // Menu List Label - adding to frame, setting size, and setting bounds
        mainFrame.add(menuLabel);
        menuLabel.setPreferredSize(menuLabelSize);
        menuLabel.setBounds(mainInsets.left + 161, mainInsets.top + 25, menuLabelSize.width, menuLabelSize.height);
        
        // Menu List JList - adding to frame, setting size, and setting bounds
        mainFrame.add(scrollPaneMenu);
        scrollPaneMenu.setPreferredSize(menuSize);
        scrollPaneMenu.setBounds(mainInsets.left + 160, mainInsets.top + 45, menuSize.width, menuSize.height);
        
        // -----------------------------------------------------------------------------------------------
        
        // HORIZONTAL SEPARATOR
        
        mainFrame.add(horizontalSeparator);
        horizontalSeparator.setPreferredSize(separatorSize);
        horizontalSeparator.setBounds(mainInsets.left + 5, mainInsets.top + 225, separatorSize.width, separatorSize.height);
        
        // -----------------------------------------------------------------------------------------------
        
        // SIDE BUTTONS FOR ORDERS
       
        // Creating Order - adding to frame, setting size, and setting bounds
        mainFrame.add(createOrder);
        createOrder.setPreferredSize(loadFileSize);
        createOrder.setBounds(mainInsets.left + 5, mainInsets.top + 250, buttonSize.width, buttonSize.height);
        createOrder.setEnabled(false);
        
        // Adding menu item to order - adding to frame, setting size, and setting bounds
        mainFrame.add(addItemOrder);
        addItemOrder.setPreferredSize(menuItemSize);
        addItemOrder.setBounds(mainInsets.left + 5, mainInsets.top + 280, buttonSize.width, buttonSize.height);
        addItemOrder.setEnabled(false);
        
        // Tending order - adding to frame, setting size, and setting bounds
        mainFrame.add(tendOrder);
        tendOrder.setPreferredSize(menuItemSize);
        tendOrder.setBounds(mainInsets.left + 5, mainInsets.top + 310, buttonSize.width, buttonSize.height);
        tendOrder.setEnabled(false);
        
        // Paying order - adding to frame, setting size, and setting bounds
        mainFrame.add(payOrder);
        payOrder.setPreferredSize(menuItemSize);
        payOrder.setBounds(mainInsets.left + 5, mainInsets.top + 340, buttonSize.width, buttonSize.height);
        payOrder.setEnabled(false);
        
        // Viewing order - adding to frame, setting size, and setting bounds
        mainFrame.add(viewOrder);
        viewOrder.setPreferredSize(menuItemSize);
        viewOrder.setBounds(mainInsets.left + 5, mainInsets.top + 370, buttonSize.width, buttonSize.height);
        viewOrder.setEnabled(false);
        
		// -----------------------------------------------------------------------------------------------
        
        // ORDER LIST
        
        // Order List Label - adding to frame, setting size, and setting bounds
        mainFrame.add(orderLabel);
        orderLabel.setPreferredSize(menuLabelSize);
        orderLabel.setBounds(mainInsets.left + 161, mainInsets.top + 230, menuLabelSize.width, menuLabelSize.height);
        
        // Menu List JList - adding to frame, setting size, and setting bounds
        mainFrame.add(scrollPaneOrder);
        scrollPaneOrder.setPreferredSize(menuSize);
        scrollPaneOrder.setBounds(mainInsets.left + 160, mainInsets.top + 250, menuSize.width, menuSize.height);
        
        // -----------------------------------------------------------------------------------------------
        
        mainFrame.setVisible(true);
        
		////////////////////////////////////////////////////////////////////////////////////////////////
        
    	// ---------------------------------------------------------------------------------------------
    	// ADDING and setting up add menu item frame GUI components
    	// ---------------------------------------------------------------------------------------------
        
        // Setting Size, Layout, and Resizable options
        addMenuItemFrame.setLayout(null);
        addMenuItemFrame.setSize(addMenuItemInsets.left + addMenuItemInsets.right + 750, addMenuItemInsets.top + addMenuItemInsets.bottom + 130);
        addMenuItemFrame.setDefaultCloseOperation(1);
        addMenuItemFrame.setResizable(true);
        
        // Type Label - adding to frame, setting size, and setting bounds
        addMenuItemFrame.add(typeLabel);
        typeLabel.setPreferredSize(labelSize);
        typeLabel.setBounds(addMenuItemInsets.left + 5, addMenuItemInsets.top + 5, labelSize.width, labelSize.height);
        
        // Name Label - adding to frame, setting size, and setting bounds
        addMenuItemFrame.add(nameLabel);
        nameLabel.setPreferredSize(labelSize);
        nameLabel.setBounds(addMenuItemInsets.left + 108, addMenuItemInsets.top + 5, labelSize.width, labelSize.height);
        
        // Cost Label - adding to frame, setting size, and setting bounds
        addMenuItemFrame.add(costLabel);
        costLabel.setPreferredSize(labelSize);
        costLabel.setBounds(addMenuItemInsets.left + 211, addMenuItemInsets.top + 5, labelSize.width, labelSize.height);
        
        // Price Label - adding to frame, setting size, and setting bounds
        addMenuItemFrame.add(priceLabel);
        priceLabel.setPreferredSize(labelSize);
        priceLabel.setBounds(addMenuItemInsets.left + 314, addMenuItemInsets.top + 5, labelSize.width, labelSize.height);
        
        // Spec One Label - adding to frame, setting size, and setting bounds
        addMenuItemFrame.add(specOneLabel);
        specOneLabel.setPreferredSize(labelSize);
        specOneLabel.setBounds(addMenuItemInsets.left + 417, addMenuItemInsets.top + 5, labelSize.width, labelSize.height);
        
        // Spec Two Label - adding to frame, setting size, and setting bounds
        addMenuItemFrame.add(specTwoLabel);
        specTwoLabel.setPreferredSize(labelSize);
        specTwoLabel.setBounds(addMenuItemInsets.left + 520, addMenuItemInsets.top + 5, labelSize.width, labelSize.height);
        
        // Spec Three Label - adding to frame, setting size, and setting bounds
        addMenuItemFrame.add(specThreeLabel);
        specThreeLabel.setPreferredSize(labelSize);
        specThreeLabel.setBounds(addMenuItemInsets.left + 623, addMenuItemInsets.top + 5, labelSize.width, labelSize.height);
        
		// -----------------------------------------------------------------------------------------------
        
        // Different menu item types - adding to frame, setting size, and setting bounds
        addMenuItemFrame.add(addTypes);
        addTypes.setPreferredSize(comboBoxSize);
        addTypes.setBounds(addMenuItemInsets.left + 5, addMenuItemInsets.top + 30, comboBoxSize.width, comboBoxSize.height);
        
		// -----------------------------------------------------------------------------------------------
        
        // Name Text Field - adding to frame, setting size, and setting bounds
        addMenuItemFrame.add(Name);
        Name.setPreferredSize(textFieldSize);
        Name.setBounds(addMenuItemInsets.left + 108, addMenuItemInsets.top + 30, textFieldSize.width, textFieldSize.height); 
        Name.setEnabled(false);
        
        // Cost Text Field - adding to frame, setting size, and setting bounds
        addMenuItemFrame.add(Cost);
        Cost.setPreferredSize(textFieldSize);
        Cost.setBounds(addMenuItemInsets.left + 211, addMenuItemInsets.top + 30, textFieldSize.width, textFieldSize.height);
        Cost.setEnabled(false);
        
        // Price Text Field - adding to frame, setting size, and setting bounds
        addMenuItemFrame.add(Price);
        Price.setPreferredSize(textFieldSize);
        Price.setBounds(addMenuItemInsets.left + 314, addMenuItemInsets.top + 30, textFieldSize.width, textFieldSize.height);
        Price.setEnabled(false);
        
        // Spec One Text Field - adding to frame, setting size, and setting bounds
        addMenuItemFrame.add(specFieldOne);
        specFieldOne.setPreferredSize(textFieldSize);
        specFieldOne.setBounds(addMenuItemInsets.left + 417, addMenuItemInsets.top + 30, textFieldSize.width, textFieldSize.height);
        specFieldOne.setEnabled(false);
        
        
        // Spec Two Text Field - adding to frame, setting size, and setting bounds
        addMenuItemFrame.add(specFieldTwo);
        specFieldTwo.setPreferredSize(textFieldSize);
        specFieldTwo.setBounds(addMenuItemInsets.left + 520, addMenuItemInsets.top + 30, textFieldSize.width, textFieldSize.height);
        specFieldTwo.setEnabled(false);
        
        // Spec Three Text Field - adding to frame, setting size, and setting bounds
        addMenuItemFrame.add(specFieldThree);
        specFieldThree.setPreferredSize(textFieldSize);
        specFieldThree.setBounds(addMenuItemInsets.left + 621, addMenuItemInsets.top + 30, textFieldSize.width, textFieldSize.height);
        specFieldThree.setEnabled(false);
        
		// -----------------------------------------------------------------------------------------------
        
        // Confirm Button - adding to frame, setting size, and setting bounds
        addMenuItemFrame.add(addConfirm);
        addConfirm.setPreferredSize(confirmSize);
        addConfirm.setBounds(addMenuItemInsets.left + 5, addMenuItemInsets.top + 55, confirmSize.width, confirmSize.height);
        addConfirm.setEnabled(false);
        
		////////////////////////////////////////////////////////////////////////////////////////////////
        
    	// ---------------------------------------------------------------------------------------------
    	// ADDING and setting up view order frame GUI components
    	// ---------------------------------------------------------------------------------------------
        
        // Setting Size, Layout, and Resizable options
        viewOrderFrame.setLayout(null);
        viewOrderFrame.setSize(addMenuItemInsets.left + addMenuItemInsets.right + menuSize.width + 25, 
        						addMenuItemInsets.top + addMenuItemInsets.bottom + menuSize.height + 110);
        viewOrderFrame.setDefaultCloseOperation(1);
        viewOrderFrame.setResizable(true);
        
		// -----------------------------------------------------------------------------------------------
        
        // ORDER ITEM LIST
        
        // Order State Label - adding to frame, setting size, and setting bounds
        viewOrderFrame.add(orderStateLabel);
        orderStateLabel.setPreferredSize(menuLabelSize);
        orderStateLabel.setBounds(mainInsets.left + 5, mainInsets.top + 5, menuLabelSize.width, menuLabelSize.height);
        
        // Order Menu Item List Label - adding to frame, setting size, and setting bounds
        viewOrderFrame.add(orderMenuLabel);
        orderMenuLabel.setPreferredSize(menuLabelSize);
        orderMenuLabel.setBounds(mainInsets.left + 7, mainInsets.top + 25, menuLabelSize.width, menuLabelSize.height);
        
        // Menu List JList - adding to frame, setting size, and setting bounds
        viewOrderFrame.add(scrollPaneOrderMenu);
        scrollPaneOrderMenu.setPreferredSize(menuSize);
        scrollPaneOrderMenu.setBounds(mainInsets.left + 5, mainInsets.top + 45, menuSize.width, menuSize.height);
        
        // Order Total Label - adding to frame, setting size, and setting bounds
        viewOrderFrame.add(orderTotalLabel);
        orderTotalLabel.setPreferredSize(menuSize);
        orderTotalLabel.setBounds(mainInsets.left + 5, mainInsets.top + 130, menuSize.width, menuSize.height);
        
        
		////////////////////////////////////////////////////////////////////////////////////////////////
        
    	// ---------------------------------------------------------------------------------------------
    	// ACTION LISTENERS for GUI
    	// ---------------------------------------------------------------------------------------------
        
        // =============================================================================================
        // CURRENCYTYPES Action Listener - When currency is selected, this will allow for the 
        // key type to be chosen, and then for the file to be chosen.
        // =============================================================================================
        currencyTypes.addActionListener(new ActionListener()
        {
			public void actionPerformed(ActionEvent e) 
			{
				String selectedItem = (String)currencyTypes.getSelectedItem();
				Currency newCurrency = null;
				
				try
				{
					newCurrency = myCurrencyFactory.createCurrency(selectedItem);
				}
				catch(IllegalArgumentException ex)
				{
					JOptionPane.showMessageDialog(null, myMenuException.getMsg(myMenuException.INVALID_INPUT), "Error", JOptionPane.ERROR_MESSAGE);
            		return;
				}
			
				if(fileLoaded)
				{
					myAppLogic.setMenuItemCurrency(newCurrency);
					
					myMenuListModel.removeAllElements();
					
	                // Add Items to Menu JList
	                for(int i = 0; i < (myAppLogic.getMenuList()).size(); i++)
	                {
	                    myMenuListModel.addElement((myAppLogic.getMenuList()).get(i));
	                }
				}
				else
				{
					(myAppLogic.getFileIO()).setCurrency(newCurrency);
	                keyTypes.setEnabled(true);
				}
				
		        costLabel.setText("Cost: " + myAppLogic.getCurrency());
		        priceLabel.setText("Price: "  + myAppLogic.getCurrency());
		        
		        myMenuList.repaint();
		        myOrderList.repaint();
			}
        });
        
        // =============================================================================================
        // KEYTYPES Action Listener - When 
        // =============================================================================================
        keyTypes.addActionListener(new ActionListener()
        {
			public void actionPerformed(ActionEvent e) 
			{
				String selectedItem = (String)keyTypes.getSelectedItem();
				Comparator<MenuItem> newComparator = myComparatorFactory.createComparator(selectedItem);
				
				if(fileLoaded)
				{
					myAppLogic.sortMenuItems(newComparator);
					
					myMenuListModel.removeAllElements();
					
	                // Add Items to JList
	                for(int i = 0; i < (myAppLogic.getMenuList()).size(); i++)
	                {
	                    myMenuListModel.addElement((myAppLogic.getMenuList()).get(i));
	                }
				}
				else
				{
					myAppLogic.setComparator(newComparator);
	                loadFile.setEnabled(true);
				}
				
		        costLabel.setText("Cost: " + myAppLogic.getCurrency());
		        priceLabel.setText("Price: "  + myAppLogic.getCurrency());
		        
		        myMenuList.repaint();
			}
        });
        
        // =============================================================================================
        // CLEARSEARCH Action Listener - Resets the menu item list
        // =============================================================================================
        clearSearch.addActionListener(new ActionListener()
        {
			public void actionPerformed(ActionEvent e) 
			{
				String selectedItem = (String)keyTypes.getSelectedItem();
				Comparator<MenuItem> newComparator = myComparatorFactory.createComparator(selectedItem);
				
				myAppLogic.sortMenuItems(newComparator);
					
				myMenuListModel.removeAllElements();
					
                // Add Items to JList
                for(int i = 0; i < (myAppLogic.getMenuList()).size(); i++)
                {
                    myMenuListModel.addElement((myAppLogic.getMenuList()).get(i));
                }
                
                myMenuList.repaint();
			}
        });
        
        
        // =============================================================================================
        // LOADFILE Action Listener - When clicked, this button will open up a file explorer so the user
        // can choose a file. AppLogic reads file, stores menu items, and then is used to grab those
        // menu items and put them in a combo box.
        // =============================================================================================
        loadFile.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) 
            {
            	Comparator<MenuItem> newComparator;
            	
            	// Grab selected item
				String selectedItem = (String)keyTypes.getSelectedItem();
                int returnVal = fileChooser.showOpenDialog(null);
                
                if(returnVal == JFileChooser.APPROVE_OPTION) 
                {
                	myAppLogic.setFile(fileChooser.getSelectedFile());
                	
                    try 
                    {
                        myAppLogic.readFile();

                    } 
                    catch (FileNotFoundException ex) 
                    {
                    	JOptionPane.showMessageDialog(null, myMenuException.getMsg(myMenuException.FILE_NOT_FOUND), "Error", JOptionPane.ERROR_MESSAGE);
                    	return;
                    }
                    catch (IllegalArgumentException ex)
                    {
                    	JOptionPane.showMessageDialog(null, myMenuException.getMsg(myMenuException.INVALID_FILE_FORMAT), "Error", JOptionPane.ERROR_MESSAGE);
                    	return;
                    }
                    catch(Exception ex)
                    {
                    	JOptionPane.showMessageDialog(null, myMenuException.getMsg(myMenuException.FILE_ERROR), "Error", JOptionPane.ERROR_MESSAGE);
                    	return;
                    }
                    
                    
                    for(int i = 0; i < (myAppLogic.getMenuList()).size(); i++)
                    {
 
                        myMenuListModel.addElement((myAppLogic.getMenuList()).get(i));
                    }
                    
                    // enable most of program
                    addMenuItem.setEnabled(true);
                    deleteMenuItem.setEnabled(true);
                    editMenuItem.setEnabled(true);
                    Search.setEnabled(true);
	                searchField.setEnabled(true);
	                clearSearch.setEnabled(true);
	                viewMenuItem.setEnabled(true);
	                createOrder.setEnabled(true);
	                addItemOrder.setEnabled(true);
	                tendOrder.setEnabled(true);
	                payOrder.setEnabled(true);
	                viewOrder.setEnabled(true);
                    
                    fileLoaded = true;
                }
                else
                {
                    JOptionPane.showMessageDialog(null, myMenuException.getMsg(myMenuException.FILE_ERROR), "Error", JOptionPane.ERROR_MESSAGE);
                }
            }       
        });
        
		// -----------------------------------------------------------------------------------------------
        
        // =============================================================================================
        // ADDMENUITEM Action Listener - When clicked, it pops up the add menu item frame.
        // =============================================================================================
        addMenuItem.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) 
            {
                addMenuItemFrame.setVisible(true);
            }       
        });
        
        // =============================================================================================
        // ADDMENUITEM Action Listener - Sets global variable, isEditing, to false when this frame is
        // closed.
        // =============================================================================================
        addMenuItemFrame.addWindowListener(new java.awt.event.WindowAdapter() 
        {
            public void windowClosing(java.awt.event.WindowEvent windowEvent) 
            {
                isEditing = false;
            }
        });
        
		// -----------------------------------------------------------------------------------------------
        
        // =============================================================================================
        // DELETEMENUITEM Action Listener - When clicked, it deletes the specified menu item from 
        // FoodStorage and its associated JList, and then repaints it.
        // =============================================================================================
        deleteMenuItem.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) 
            {
            	int Index = -1;
            	int confirmDelete = -1;
            	
            	confirmDelete = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete this item?", "Confirm Delete", JOptionPane.YES_NO_CANCEL_OPTION);
	
            	if(confirmDelete == JOptionPane.YES_OPTION)
            	{
            		Index = myMenuList.getSelectedIndex();
            		
                	try 
                    {
                    		myAppLogic.deleteMenuItem(Index);
                    		myMenuListModel.remove(Index);
                    }
                	catch (IndexOutOfBoundsException ex)
                	{
                		JOptionPane.showMessageDialog(null, myMenuException.getMsg(myMenuException.MENU_ITEM_NOT_FOUND), "Error", JOptionPane.ERROR_MESSAGE);
                		return;
                	}
                	catch (Exception ex)
                	{
                		JOptionPane.showMessageDialog(null, ex.getMessage());
                		return;
                	}
                	
                	myMenuList.repaint();
            	}
            }       
        });
        
		// -----------------------------------------------------------------------------------------------
        
        addTypes.addActionListener(new ActionListener()
        {
			public void actionPerformed(ActionEvent e) 
			{
				// Grab selected item
				String selectedItem = (String)addTypes.getSelectedItem();
				
				// Unique field formats
				// --> Appetizer,Name,Cost,Price,Is Cold,Prep Time,Country of Origin
				// --> Meal,Name,Cost,Price,Is Salad,Cook Time,Is Organic                                 
				// --> Dessert,Name,Cost,Price,Is Cake,Initial Servings,Is Special                        
				// --> Soda,Name,Cost,Price,Flavor,Ice Cream,Has Whipped Cream                            
				// --> Milkshake,Name,Cost,Price,Flavor,Ice Cream,Malt Powder Scoops                      
				
				if(selectedItem.equalsIgnoreCase("APPETIZER"))
				{
					// Set Unique Labels
					specOneLabel.setText("Is Cold:");
					specTwoLabel.setText("Prep Time:");
					specThreeLabel.setText("Country of Origin:");
				}
				else if(selectedItem.equalsIgnoreCase("MEAL"))
				{
					// Set Unique Labels
					specOneLabel.setText("Is Salad:");
					specTwoLabel.setText("Cook Time:");
					specThreeLabel.setText("Is Organic:");
				}
				else if(selectedItem.equalsIgnoreCase("DESSERT"))
				{
					// Set Unique Labels
					specOneLabel.setText("Is Cake:");
					specTwoLabel.setText("Initial Servings:");
					specThreeLabel.setText("Is Special:");
				}
				else if(selectedItem.equalsIgnoreCase("SODA"))
				{
					// Set Unique Labels
					specOneLabel.setText("Flavor:");
					specTwoLabel.setText("Ice Cream:");
					specThreeLabel.setText("Has Whipped Cream:");
				}
				else if(selectedItem.equalsIgnoreCase("MILKSHAKE"))
				{
					// Set Unique Labels
					specOneLabel.setText("Flavor:");
					specTwoLabel.setText("Ice Cream:");
					specThreeLabel.setText("Number of Malt Powder Scoops:");
				}
				else
				{
					JOptionPane.showMessageDialog(null, myMenuException.getMsg(myMenuException.INVALID_TYPE), "Error", JOptionPane.ERROR_MESSAGE);
            		return;
				}
				
				// Set Enabled
				Name.setEnabled(true);
				Cost.setEnabled(true);
				Price.setEnabled(true);
				specFieldOne.setEnabled(true);
				specFieldTwo.setEnabled(true);
				specFieldThree.setEnabled(true);
				addConfirm.setEnabled(true);	
			}
        });
        
		// -----------------------------------------------------------------------------------------------
        
        // =============================================================================================
        // ADDCONFIRM Action Listener - Tries to add a new Menu Item into FoodStorage with user input.
        // If any of the values inputted by the user are invalid, then it will notify the user and the
        // Menu Item will not be added. If successful, closes frame and repopulates ComboBox.
        // =============================================================================================
        addConfirm.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) 
            {
                String itemData = "";
                
                // Collection of error messages
                String Errors = "";
                JLabel errorLabel;
                
                // grab text
                String typeText = (String)addTypes.getSelectedItem();
                String costText = Cost.getText();
                String priceText = Price.getText();
                String specFieldOneText = specFieldOne.getText();
                String specFieldTwoText = specFieldTwo.getText();
                String specFieldThreeText = specFieldThree.getText();
               
                // cost and price validation
                boolean costValid = Pattern.matches(floatRegex, costText);
                boolean priceValid = Pattern.matches(floatRegex, priceText);
                
				// Menu Item formats
				// --> Appetizer,Name,Cost,Price,Is Cold,Prep Time,Country of Origin
				// --> Meal,Name,Cost,Price,Is Salad,Cook Time,Is Organic                                 
				// --> Dessert,Name,Cost,Price,Is Cake,Initial Servings,Is Special                        
				// --> Soda,Name,Cost,Price,Flavor,Ice Cream,Has Whipped Cream                            
				// --> Milkshake,Name,Cost,Price,Flavor,Ice Cream,Malt Powder Scoops 
                
                // Unique Menu Item fields data types
            	// App -> boolean, int, string
            	// Meal -> boolean, int, boolean
            	// Dessert -> boolean, int, boolean
            	// Soda -> string, string, boolean
            	// Milkshake -> string, string, int
      
                // unique field validation
                boolean specOneBoolValid = specFieldOneText.equalsIgnoreCase("T") || specFieldOneText.equalsIgnoreCase("F");
                boolean specOneTextValid = !specFieldOneText.trim().equals("");
                boolean specTwoIntValid = Pattern.matches(intRegex, specFieldTwoText);
                boolean specTwoTextValid = !specFieldTwoText.trim().equals("");
                boolean specThreeIntValid = Pattern.matches(intRegex, specFieldThreeText);
                boolean specThreeBoolValid = specFieldThreeText.equalsIgnoreCase("T") || specFieldThree.getText().equalsIgnoreCase("F");
                boolean specThreeTextValid = !specFieldThreeText.trim().equals("");
                
                
                
                // total validation - innocent until proven guilty
                boolean canAdd = true;
                
                // cost and price validation
                if(!costValid || costText.trim().equals(""))
                    Errors += myMenuException.getMsg(myMenuException.INVALID_COST) + "<br>";
                
                if(!priceValid || priceText.trim().equals(""))
                	Errors += myMenuException.getMsg(myMenuException.INVALID_PRICE)+ "<br>";
                
                // unique validation
				if(typeText.equalsIgnoreCase("APPETIZER"))
				{	
					if(!specOneBoolValid || !specOneTextValid)
						Errors += myMenuException.getMsg(myMenuException.INVALID_BOOL_COLD)+ "<br>";
					
					if(!specTwoIntValid || !specTwoTextValid)
						Errors += myMenuException.getMsg(myMenuException.INVALID_INT_PREP)+ "<br>";
					
					if(!specThreeTextValid)
						Errors += myMenuException.getMsg(myMenuException.INVALID_STRING_COUNTRY)+ "<br>";
					
					canAdd = costValid && priceValid && specOneBoolValid && specTwoIntValid;
				}
				else if(typeText.equalsIgnoreCase("MEAL"))
				{
					if(!specOneBoolValid || !specOneTextValid)
						Errors += myMenuException.getMsg(myMenuException.INVALID_BOOL_SALAD)+ "<br>";
					
					if(!specTwoIntValid || !specTwoTextValid)
						Errors += myMenuException.getMsg(myMenuException.INVALID_INT_COOK)+ "<br>";
					
					if(!specThreeBoolValid || !specThreeTextValid)
						Errors += myMenuException.getMsg(myMenuException.INVALID_BOOL_ORGANIC)+ "<br>";
					
					canAdd = costValid && priceValid && specOneBoolValid && specTwoIntValid && specThreeBoolValid;
				}
				else if(typeText.equalsIgnoreCase("DESSERT"))
				{
					if(!specOneBoolValid || !specOneTextValid)
						Errors += myMenuException.getMsg(myMenuException.INVALID_BOOL_CAKE)+ "<br>";
					
					if(!specTwoIntValid || !specTwoTextValid)
						Errors += myMenuException.getMsg(myMenuException.INVALID_INT_SERVINGS)+ "<br>";
					
					if(!specThreeBoolValid || !specThreeTextValid)
						Errors += myMenuException.getMsg(myMenuException.INVALID_BOOL_SPECIAL)+ "<br>";
					
					canAdd = costValid && priceValid && specOneBoolValid && specTwoIntValid && specThreeBoolValid;
				}
				else if(typeText.equalsIgnoreCase("SODA"))
				{
					if(!specOneTextValid)
						Errors += myMenuException.getMsg(myMenuException.INVALID_STRING_FLAVOR)+ "<br>";
					if(!specTwoTextValid)
						Errors += myMenuException.getMsg(myMenuException.INVALID_STRING_ICECREAM)+ "<br>";
					if(!specThreeBoolValid || !specThreeTextValid)
						Errors += myMenuException.getMsg(myMenuException.INVALID_BOOL_WHIPPED)+ "<br>";
					
					canAdd = costValid && priceValid && specThreeBoolValid;
				}
				else if(typeText.equalsIgnoreCase("MILKSHAKE"))
				{
					if(!specOneTextValid)
						Errors += myMenuException.getMsg(myMenuException.INVALID_STRING_FLAVOR)+ "<br>";
					if(!specTwoTextValid)
						Errors += myMenuException.getMsg(myMenuException.INVALID_STRING_ICECREAM)+ "<br>";
					if(!specThreeIntValid || !specThreeTextValid)
						Errors += myMenuException.getMsg(myMenuException.INVALID_INT_SCOOPS)+ "<br>";
					
					canAdd = costValid && priceValid && specThreeIntValid;
				}
				else
				{	
					// Notify User If Not Valid Type
					JOptionPane.showMessageDialog(null, myMenuException.getMsg(myMenuException.INVALID_TYPE), "Error", JOptionPane.ERROR_MESSAGE);
					
					// Reset Error
					Errors = "";
					
            		return;
				}
              
				// Populate string data
                itemData = (String)addTypes.getSelectedItem() + "," + Name.getText() + "," + Cost.getText() + "," + Price.getText() + "," +
                            specFieldOne.getText() + "," + specFieldTwo.getText() + "," + specFieldThree.getText();
                
                // Notify User of Errors
                if(!canAdd)
                {
                	errorLabel = new JLabel("<html>" + Errors + "</html>");
                	
                	JOptionPane.showMessageDialog(null, errorLabel, "Error", JOptionPane.ERROR_MESSAGE);
            		return;
                }

                // Edit Mode
                if(isEditing)
                {
					try
					{
						myAppLogic.editMenuItem(myMenuList.getSelectedIndex(), itemData);
					}
                	catch (IndexOutOfBoundsException ex)
                	{
                		JOptionPane.showMessageDialog(null, myMenuException.getMsg(myMenuException.MENU_ITEM_NOT_FOUND), "Error", JOptionPane.ERROR_MESSAGE);
                		return;
                	}
                	catch (Exception ex)
                	{
                		JOptionPane.showMessageDialog(null, ex.getMessage());
                		return;
                	}
					
                    isEditing = false;
                }
                else
                {
                    // Create Item
                    myAppLogic.createMenuItem(itemData);
                }
                
                // Remove Items from JList
                myMenuListModel.removeAllElements();
                
                // Add Items to JList
                for(int i = 0; i < (myAppLogic.getMenuList()).size(); i++)
                {
                    myMenuListModel.addElement((myAppLogic.getMenuList()).get(i));
                }
                
                // Reset Text
                Name.setText("");
                Cost.setText("");
                Price.setText("");
                specFieldOne.setText("");
                specFieldTwo.setText("");
                specFieldThree.setText("");
                
                // Hide Frame
                addMenuItemFrame.setVisible(false);
            }       
        });
        
        // =============================================================================================
        // EDITMENUITEM Button Action Listener - Reuses AddMenuItem frame, but sets some basic fields
        // for the user.
        // =============================================================================================
        editMenuItem.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) 
            {
                isEditing = true;
                
				MenuItem selectedMenuItem = (MenuItem)myMenuList.getSelectedValue();
				String menuItemType = "";
				String[] uniqueSpecs;
				
				if(selectedMenuItem == null)
				{
					JOptionPane.showMessageDialog(null, myMenuException.getMsg(myMenuException.MENU_ITEM_NOT_FOUND), "Error", JOptionPane.ERROR_MESSAGE);
            		return;
				}
				
				menuItemType = selectedMenuItem.printType();
				
				// Set combo box
				if(menuItemType.equalsIgnoreCase("APPETIZER"))
				{
					addTypes.setSelectedIndex(0);
				}
				else if(menuItemType.equalsIgnoreCase("MEAL"))
				{
					addTypes.setSelectedIndex(1);
				}
				else if(menuItemType.equalsIgnoreCase("DESSERT"))
				{
					addTypes.setSelectedIndex(2);
				}
				else if(menuItemType.equalsIgnoreCase("SODA"))
				{
					addTypes.setSelectedIndex(3);
				}
				else if(menuItemType.equalsIgnoreCase("MILKSHAKE"))
				{
					addTypes.setSelectedIndex(4);
				}
				else
				{
					JOptionPane.showMessageDialog(null, myMenuException.getMsg(myMenuException.INVALID_TYPE), "Error", JOptionPane.ERROR_MESSAGE);
            		return;
				}
                
                // Set Fields 
                Name.setText(selectedMenuItem.getName());
                Cost.setText("" + selectedMenuItem.getCost());
                Price.setText("" + selectedMenuItem.getPrice());
                
                specFieldOne.setText("");
                specFieldTwo.setText("");
                specFieldThree.setText("");
                
                // Reuse same frame!
                addMenuItemFrame.setVisible(true);
            }       
        });
        
        // =============================================================================================
        // VIEWMENUITEM Button Action Listener - Grabs unique navigation and 
        // =============================================================================================
        viewMenuItem.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) 
            {
				MenuItem selectedMenuItem = (MenuItem)myMenuList.getSelectedValue();
				
				// Is anything selected?
				if(selectedMenuItem == null)
				{
					JOptionPane.showMessageDialog(null, myMenuException.getMsg(myMenuException.MENU_ITEM_NOT_FOUND), "Error", JOptionPane.ERROR_MESSAGE);
            		return;
				}
				
				// Grab MenuItem info and three unique attributes
				String menuItemInfo = String.format(selectedMenuItem + selectedMenuItem.uniqueSpecs());
				// Grab Unique navigation
				String menuItemNav = "<b>" + getMenuItemNav(selectedMenuItem) + "</b> <br>";
				// Grab MenuItem Type
				String menuItemType = "<b>TYPE: </b>" + selectedMenuItem.printType() + "<br>";	
				
				// Make label and set font
				JLabel menuItemLabel = new JLabel("<html>" + menuItemType + menuItemNav + menuItemInfo + "</html>");
				menuItemLabel.setFont(listFont);
				 
				JOptionPane.showMessageDialog(null, menuItemLabel, "Menu Item Info", JOptionPane.PLAIN_MESSAGE);
        		return;
            }       
        });
        
        // =============================================================================================
        // SEARCHBUTTON Action Listener - Finds menu item by name. Removes everything from JList and 
        // then adds the found item to the Model. Doesn't find all items if there are duplicates, just 
        // the first one it finds.
        // =============================================================================================
        Search.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) 
            {
            	String searchName = searchField.getText();
            	int Index = 0;
            	
            	try
            	{
            		Index = myAppLogic.findMenuItem(searchName);
            	}
            	catch(IllegalArgumentException ex)
            	{
            		JOptionPane.showMessageDialog(null, myMenuException.getMsg(myMenuException.MENU_ITEM_NOT_FOUND), "Error", JOptionPane.ERROR_MESSAGE);
            		return;
            	}
            	catch(Exception ex)
            	{
            		JOptionPane.showMessageDialog(null, ex.getMessage());
            		return;
            	}
            	
            	// remove everything and adds it JList and repaints it.
            	myMenuListModel.removeAllElements();
            	myMenuListModel.addElement(myAppLogic.getMenuList().get(Index));
            	myMenuList.repaint();
            }       
        });
        
        // =============================================================================================
        // CREATEORDER Action Listener - Creates a new order via AppLogic and adds it to OrderStorage,
        // and then adds it to the JList model.
        // =============================================================================================
        createOrder.addActionListener(new ActionListener()
        {
			public void actionPerformed(ActionEvent e) 
			{
				Order myOrder = myAppLogic.createOrder();
				myOrderListModel.addElement(myOrder);
				myOrderList.repaint();
			}
        });
        
        // =============================================================================================
        // ADDITEMORDER Action Listener - Simply adds the menu item to the order and changes the item's 
        // state to "In Process" if it isn't in it already.
        // =============================================================================================
        addItemOrder.addActionListener(new ActionListener()
        {
			public void actionPerformed(ActionEvent e) 
			{
				MenuItem selectedItem = (MenuItem)myMenuList.getSelectedValue();
				Order selectedOrder = (Order)myOrderList.getSelectedValue();
				
				if(selectedItem == null)
				{
					JOptionPane.showMessageDialog(null, myMenuException.getMsg(myMenuException.MENU_ITEM_NOT_FOUND), "Error", JOptionPane.ERROR_MESSAGE);
            		return;
				}
				
				if(selectedOrder == null)
				{
					JOptionPane.showMessageDialog(null, myMenuException.getMsg(myMenuException.ORDER_NOT_FOUND), "Error", JOptionPane.ERROR_MESSAGE);
            		return;
				}
				
				selectedOrder.addItem(selectedItem);
				
				myOrderList.repaint();
			}
        });
        
        // =============================================================================================
        // TENDORDER Action Listener - Simply changes the selected order's state to "Tended".
        // =============================================================================================
        tendOrder.addActionListener(new ActionListener()
        {
			public void actionPerformed(ActionEvent e) 
			{
				Order selectedOrder = (Order)myOrderList.getSelectedValue();
				
				if(selectedOrder == null)
				{
					JOptionPane.showMessageDialog(null, myMenuException.getMsg(myMenuException.ORDER_NOT_FOUND), "Error", JOptionPane.ERROR_MESSAGE);
            		return;
				}
				
				selectedOrder.tendOrder();
				
				myOrderList.repaint();
			}
        });
        
        // =============================================================================================
        // PAYORDER Action Listener - Simply changes the selected order's state to "Paid".
        // =============================================================================================
        payOrder.addActionListener(new ActionListener()
        {
			public void actionPerformed(ActionEvent e) 
			{
				Order selectedOrder = (Order)myOrderList.getSelectedValue();
				
				if(selectedOrder == null)
				{
					JOptionPane.showMessageDialog(null, myMenuException.getMsg(myMenuException.ORDER_NOT_FOUND), "Error", JOptionPane.ERROR_MESSAGE);
            		return;
				}
				
				selectedOrder.payOrder();
				
				myOrderList.repaint();
			}	
        });
        
        // =============================================================================================
        // VIEWORDER Action Listener - opens up a new frame to view order details, including state
        // and menu items.
        // =============================================================================================
        viewOrder.addActionListener(new ActionListener()
        {
			public void actionPerformed(ActionEvent e) 
			{	
				Order selectedOrder = (Order)myOrderList.getSelectedValue();
				ArrayList<MenuItem> selectedMenuItems;
				ArrayList<Integer> numMenuItems;
				DecimalFormat myCurrencyFormat;
				
				if(selectedOrder == null)
				{
					JOptionPane.showMessageDialog(null, myMenuException.getMsg(myMenuException.ORDER_NOT_FOUND), "Error", JOptionPane.ERROR_MESSAGE);
            		return;
				}
				
				orderStateLabel.setText("STATE: " + selectedOrder.getCurrentState());
				orderTotalLabel.setText("TOTAL: " + selectedOrder.printTotal());
				
				selectedMenuItems = selectedOrder.getMyMenuItems();
				numMenuItems = selectedOrder.getNumMenuItems();
				
				// Remove All Prior Elements
				myOrderMenuListModel.removeAllElements();
				
				// Get currency format
				myCurrencyFormat = selectedOrder.createCurrencyFormat();
				
				String myMenuInfo = "";
				
				// Re-add ->
				// "Menu Item Name", "Price", "No.", "Subtotal"
				for(int i = 0; i < selectedMenuItems.size(); i++)
				{
					myMenuInfo = String.format(menuAlignFormat, (selectedMenuItems.get(i)).getName(), 
																myCurrencyFormat.format((selectedMenuItems.get(i)).getPrice()), 
																numMenuItems.get(i), 
																myCurrencyFormat.format((numMenuItems.get(i)) * (selectedMenuItems.get(i).getPrice())));
					
					myOrderMenuListModel.addElement(myMenuInfo);
				}
				
				 viewOrderFrame.setVisible(true);
			}
        });
        
		// -----------------------------------------------------------------------------------------------
    }
    
    // =============================================================================================
    // GETMENUITEMNAV() creates a unique navigation for each menu item type during item viewing.
    // =============================================================================================
    public static String getMenuItemNav(MenuItem selectedMenuItem)
    {
    	String newLineMark = System.getProperty("line.separator");
    	String leftAlignFormat = "| %-20s | %-7s | %-7s | %-7s | %-7s | %-7s | %-7s |" + newLineMark;
       	String menuItemMidNav = "";
    	
		// Menu Item formats
		// --> Appetizer,Name,Cost,Price,Is Cold,Prep Time,Country of Origin
		// --> Meal,Name,Cost,Price,Is Salad,Cook Time,Is Organic                                 
		// --> Dessert,Name,Cost,Price,Is Cake,Initial Servings,Is Special                        
		// --> Soda,Name,Cost,Price,Flavor,Ice Cream,Has Whipped Cream                            
		// --> Milkshake,Name,Cost,Price,Flavor,Ice Cream,Malt Powder Scoops 
       	
    	if(selectedMenuItem instanceof Appetizer)
    	{
    		menuItemMidNav += menuItemMidNav.format(leftAlignFormat,  "Name", "Cost", "Price", "Profit", "Is Cold", "Prep", "Country");
    	}
    	else if(selectedMenuItem instanceof Meal)
    	{
    		menuItemMidNav += menuItemMidNav.format(leftAlignFormat,  "Name", "Cost", "Price", "Profit", "Is Salad", "Cook", "Is Org.");
    	}
    	else if(selectedMenuItem instanceof Dessert)
    	{
    		menuItemMidNav += menuItemMidNav.format(leftAlignFormat,  "Name", "Cost", "Price", "Profit", "Is Cake", "Servings", "Is Spe.");
    	}
    	else if(selectedMenuItem instanceof IceCream)
    	{
    		menuItemMidNav += menuItemMidNav.format(leftAlignFormat,  "Name", "Cost", "Price", "Profit", "Flavor", "Ice Cream", "Has Cr. / # Scoops");
    	}
    	
    	return menuItemMidNav;
    }
}
