package MenuItemSystem;

import java.text.DecimalFormat;
// =====================================================================================================
// Appetizer.java
// -----------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-426: Advanced Programming
// Date Created: 09/12/13
// Last Modified: 12/01/13
// -----------------------------------------------------------------------------------------------------
// Appetizer is the child of MenuItem. Dessert itself defines isCold, prepTime, and originCountry. It 
// can be constructed normally, or by using the methods setData() and parseData(), which uses a comma 
// delimited string to parse and set its attributes.
// =====================================================================================================

public class Appetizer extends MenuItem
{
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//-------------------------------------------------------------------------------------------------
	// DECLARING attributes
	//-------------------------------------------------------------------------------------------------
	
	private boolean isCold;
	private int prepTime;
	private String originCountry;
	
	///////////////////////////////////////////////////////////////////////////////////////////////////
	
	//-------------------------------------------------------------------------------------------------
	// CONSTRUCTORS
	//-------------------------------------------------------------------------------------------------
	
	public Appetizer() 
	{
		super();
	}
	
	public Appetizer(String newName, float newCost, float newPrice, boolean newIsCold, int newPrepTime,
			String newOriginCountry) 
	{
		super(newName, newCost, newPrice);
		setIsCold(newIsCold);
		setPrepTime(newPrepTime);
		setOriginCountry(newOriginCountry);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// METHODS that parse a comma delimited string and sets attributes accordingly.
	//--------------------------------------------------------------------------------------------------
	
	// =================================================================================================
	// PARSEDATA() splits the string into an array, makes sure the right amount of attributes are 
	// present, then uses setDate() to set attributes.
	// =================================================================================================
	protected void parseData(String Data) throws NumberFormatException, IllegalArgumentException
	{	
		String[] menuData = Data.split(",");
	
		if(menuData.length == 6)
		{
			super.setData(menuData);
			this.setData(menuData);
		}
		else		
		{
			throw new IllegalArgumentException(myMenuException.getMsg(myMenuException.INVALID_FILE_FORMAT));
		}
	}
	
	// =================================================================================================
	// SETDATA() parses and sets members of the string array into the appropriate data type  for each 
	// attribute
	// =================================================================================================
	protected void setData(String[] menuData)
	{		
		if(menuData[3].equalsIgnoreCase("T") || menuData[3].equalsIgnoreCase("True"))
		{
			setIsCold(true);
		}
		else if(menuData[3].equalsIgnoreCase("F") || menuData[3].equalsIgnoreCase("False"))
		{
			setIsCold(false);
		}
		else
		{
			throw new IllegalArgumentException(myMenuException.getMsg(myMenuException.INVALID_INPUT));
		}
		
		setPrepTime(Integer.parseInt(menuData[4]));
		
		setOriginCountry(menuData[5]);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// =================================================================================================
	// UNIQUESPECS() prints out the three unique specifications for each object
	// =================================================================================================
	public String uniqueSpecs()
	{
		String newLineMark = System.getProperty("line.separator");
		String leftAlignFormat = " %-7s | %-7s | %-7s |" + newLineMark;
		
		return String.format(leftAlignFormat, "" + isCold, "" + prepTime, originCountry);
	}
	
	// =================================================================================================
	// PRINTTYPE() prints type of menuitem so we don't have to do an instanceof
	// =================================================================================================
	public String printType()
	{
		return "APPETIZER";
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// GETTERS and SETTERS for isCold, prepTime, and originCountry
	//--------------------------------------------------------------------------------------------------

	protected boolean isCold() 
	{
		return isCold;
	}

	protected int getPrepTime()
	{
		return prepTime;
	}
	
	protected String getOriginCountry()
	{
		return originCountry;
	}
	
	protected void setIsCold(boolean newIsCold) 
	{
		isCold = newIsCold;
	}
	
	protected void setPrepTime(int newPrepTime)
	{
		prepTime = newPrepTime;
	}

	protected void setOriginCountry(String newOriginCountry)
	{
		originCountry = newOriginCountry;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
}
