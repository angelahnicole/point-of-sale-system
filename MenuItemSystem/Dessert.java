package MenuItemSystem;

// =====================================================================================================
// Dessert.java
// -----------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-426: Advanced Programming
// Date Created: 09/12/13
// Last Modified: 12/01/13
// -----------------------------------------------------------------------------------------------------
// Dessert is the child of MenuItem. Dessert itself defines isCake, initServings, and isSpecial. It can 
// be constructed normally, or by using the methods setData() and parseData(), which uses a comma 
// delimited string to parse and set its attributes.
// =====================================================================================================

public class Dessert extends MenuItem
{
	////////////////////////////////////////////////////////////////////////////////////////////////////

	//--------------------------------------------------------------------------------------------------
	// DECLARING attributes
	//--------------------------------------------------------------------------------------------------
	
	private boolean isCake;
	private int initServings;
	private boolean isSpecial;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// CONSTRUCTORS
	//--------------------------------------------------------------------------------------------------
	
	public Dessert() 
	{
		super();
	}
	
	public Dessert(String newName, float newCost, float newPrice, boolean newIsCake, int newInitServings,
			boolean newIsSpecial) 
	{
		super(newName, newCost, newPrice);
		setIsCake(newIsCake);
		setInitServings(newInitServings);
		setIsSpecial(newIsSpecial);
		
		myMenuException = new MenuExceptionCode();
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// METHODS that parse a comma delimited string and sets attributes accordingly.
	//--------------------------------------------------------------------------------------------------

	// =================================================================================================
	// PARSEDATA() splits the string into an array, makes sure the right amount of attributes are 
	// present, then uses setDate() to set attributes.
	// =================================================================================================
	protected void parseData(String Data) throws NumberFormatException, IllegalArgumentException
	{	
		String[] menuData = Data.split(",");
		
		if(menuData.length == 6)
		{
			super.setDessertData(menuData);
			this.setData(menuData);
		}
		else
		{
			throw new IllegalArgumentException(myMenuException.getMsg(myMenuException.INVALID_FILE_FORMAT));
		}
	}
	
	// =================================================================================================
	// SETDATA() parses and sets members of the string array into the appropriate data type for each 
	// attribute
	// =================================================================================================
	protected void setData(String[] menuData) throws NumberFormatException, IllegalArgumentException
	{
		MenuExceptionCode myMenuException = new MenuExceptionCode();
		
		if(menuData[3].equalsIgnoreCase("T") || menuData[3].equalsIgnoreCase("True"))
		{
			setIsCake(true);
		}
		else if(menuData[3].equalsIgnoreCase("F") || menuData[3].equalsIgnoreCase("False"))
		{
			setIsCake(false);
		}
		else
		{
			throw new IllegalArgumentException(myMenuException.getMsg(myMenuException.INVALID_FILE_FORMAT));
		}
		
		setInitServings(Integer.parseInt(menuData[4]));
		
		if(menuData[5].equalsIgnoreCase("T") || menuData[5].equalsIgnoreCase("True"))
		{
			setIsSpecial(true);
		}
		else if(menuData[5].equalsIgnoreCase("F") || menuData[5].equalsIgnoreCase("False"))
		{
			setIsSpecial(false);
		}
		else
		{
			throw new IllegalArgumentException(myMenuException.getMsg(myMenuException.INVALID_FILE_FORMAT));
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// =================================================================================================
	// UNIQUESPECS() prints out the three unique specifications for each object
	// =================================================================================================
	public String uniqueSpecs()
	{
		String newLineMark = System.getProperty("line.separator");
		String leftAlignFormat = "%-7s | %-7s | %-7s |" + newLineMark;
		
		return String.format(leftAlignFormat, "" + isCake, "" + initServings, "" + isSpecial);
	}
	
	// =================================================================================================
	// PRINTTYPE() prints type of menuitem so we don't have to do an instanceof
	// =================================================================================================
	public String printType()
	{
		return "DESSERT";
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// GETTERS and SETTERS for isCake, initServings, and isSpecial
	//--------------------------------------------------------------------------------------------------

	protected boolean isCake() 
	{
		return isCake;
	}

	protected int getInitServings() 
	{
		return initServings;
	}
	
	protected boolean isSpecial()
	{
		return isSpecial;
	}
	
	protected void setIsCake(boolean newIsCake) 
	{
		isCake = newIsCake;
	}

	protected void setInitServings(int newInitServings) 
	{
		initServings = newInitServings;
	}
	
	protected void setIsSpecial(boolean newIsSpecial)
	{
		isSpecial = newIsSpecial;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
}
