package MenuItemSystem;

// =====================================================================================================
// MenuItemFactory.java
//------------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-426: Advanced Programming
// Date Created: 09/30/13
// Last Modified: 10/01/13
// -----------------------------------------------------------------------------------------------------
// MenuItemFactory is a factory class that creates MenuItem objects.
// =====================================================================================================

public class MenuItemFactory
{
	////////////////////////////////////////////////////////////////////////////////////////////////////

	//--------------------------------------------------------------------------------------------------
	// DECLARING attribute
	//--------------------------------------------------------------------------------------------------
	
	// Provides exception messages by code
	private MenuExceptionCode myMenuException;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// CONSTRUCTOR
	//--------------------------------------------------------------------------------------------------
	
	public MenuItemFactory()
	{
		myMenuException = new MenuExceptionCode();
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// =============================================================================================
	// CREATEMENUITEM() creates and returns a MenuItem, making use of each MenuItem object's 
	// parseData() method, which takes in a string and populates each item accordingly.
	// =============================================================================================
	public MenuItem createMenuItem(String Data) throws NumberFormatException, IllegalArgumentException
	{
		String[] menuData = Data.split(",");
		MenuItem myMenuItem;
		
		if(menuData[0].equalsIgnoreCase("APPETIZER"))
		{
			Data = Data.substring(10);
			myMenuItem = new Appetizer();
			myMenuItem.parseData(Data);
		}
		else if(menuData[0].equalsIgnoreCase("MEAL"))
		{
			Data = Data.substring(5);
			myMenuItem = new Meal();
			myMenuItem.parseData(Data);
		}
		else if(menuData[0].equalsIgnoreCase("DESSERT"))
		{
			Data = Data.substring(8);
			myMenuItem = new Dessert();
			myMenuItem.parseData(Data);
		}
		// The factory class needs to know if it's SODA or MILKSHAKE, so we don't substring it
		else if(menuData[0].equalsIgnoreCase("SODA") || menuData[0].equalsIgnoreCase("MILKSHAKE"))
		{
			myMenuItem = new IceCream();
			myMenuItem.parseData(Data);
		}
		else
		{
			throw new IllegalArgumentException(myMenuException.getMsg(myMenuException.INVALID_INPUT)); 
		}
		
		return myMenuItem;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
}
