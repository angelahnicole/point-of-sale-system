package MenuItemSystem;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;

// =====================================================================================================
// FileIO.java
// -----------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-426: Advanced Programming
// Date Created: 09/12/13
// Last Modified: 11/10/13
// -----------------------------------------------------------------------------------------------------
// FileIO takes in FoodStorage and File objects and populates FoodStorage's MenuList by reading in a 
// file. 
// =====================================================================================================

public class FileIO 
{
	////////////////////////////////////////////////////////////////////////////////////////////////////

	//--------------------------------------------------------------------------------------------------
	// DECLARING attributes
	//--------------------------------------------------------------------------------------------------
	
	// Main attributes
	private FoodStorage myFoodStorage;
	private File myFile;
	
	// Extra functionality 
	private Currency myCurrency;
	
	// Object factories
	private ComparatorFactory myComparatorBuilder;
	private MenuItemFactory myMenuItemBuilder;
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// CONSTRUCTOR
	//--------------------------------------------------------------------------------------------------
	
	public FileIO(File newFile, FoodStorage newFoodStorage)
	{
		setFile(newFile);
		setFoodStorage(newFoodStorage);
		myComparatorBuilder = new ComparatorFactory();
		myMenuItemBuilder = new MenuItemFactory();
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// =================================================================================================
	// READFILE() utilizes a file object that has a file that is formatted:
	// --> First Line: Comparator type name.
	// --> Lines after: Menu Item name, then MenuItem object attributes thereafter (delimited by commas)
	// The method utilizes Comparator and MenuItem factories in order to create each object type, and 
	// then sets the desired Currency to the MenuItem. It also adds each MenuItem to the FoodStorage's
	// MenuList after each iteration, and it also initially creates and sets the FoodStorage's 
	// Comparator.
	// =================================================================================================
	public void readFile() throws FileNotFoundException, NumberFormatException, IllegalArgumentException
	{	
		ArrayList<MenuItem> tempList = new ArrayList<MenuItem>();
		MenuItem myMenuItem;
		Comparator<MenuItem> myComparator = myFoodStorage.getComparator();
		String currentLine = "";
		int lineNumber = 0;
		
		Scanner myScan = new Scanner(myFile);
		
		while(myScan.hasNextLine())
		{
			currentLine = myScan.nextLine();
			
			// Comparator Type
			if(lineNumber == 0)
			{
				if(myComparator == null)
					myComparator = myComparatorBuilder.createComparator(currentLine);
				myFoodStorage.setComparator(myComparator);
			}
			// MenuItem type
			else
			{
				myMenuItem = myMenuItemBuilder.createMenuItem(currentLine);
				myMenuItem.setCurrency(myCurrency);
				tempList.add(myMenuItem);
			}
			
			lineNumber++;
		}
		
		// If an error occurs during reading, it won't input the garbage into FoodStorage. 
		// If it has gotten this far, then no errors have occurred, and thus no garbage.
		for(int i=0; i < tempList.size(); i++)
		{
			myFoodStorage.addItem(tempList.get(i));
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// GETTERS and SETTERS for myFoodStorage and myFile
	//--------------------------------------------------------------------------------------------------
	
	protected FoodStorage getFoodStorage()
	{
		return myFoodStorage;
	}

	protected File getMyFile()
	{
		return myFile;
	}
	
	protected Currency getCurrency()
	{
		return myCurrency;
	}

	protected void setFoodStorage(FoodStorage newFoodStorage)
	{
		myFoodStorage = newFoodStorage;
	}

	protected void setFile(File newFile)
	{
		myFile = newFile;
	}
	
	protected void setCurrency(Currency newCurrency)
	{
		myCurrency = newCurrency;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
}
