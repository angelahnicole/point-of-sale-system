package MenuItemSystem;

// =====================================================================================================
// NameComparator.java
// -----------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-426: Advanced Programming
// Date Created: 09/30/13
// Last Modified: 09/30/13
// -----------------------------------------------------------------------------------------------------
// NameComparator is a child of Comparator. It uses the parent class, which takes in an object to 
// compare others to (This child uses MenuItem objects). Its primary use is to compare MenuItem object's
// names.
// =====================================================================================================

public class NameComparator extends Comparator<MenuItem>
{
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// CONSTRUCTORS
	//--------------------------------------------------------------------------------------------------
	
	public NameComparator() {}
	
	public NameComparator(MenuItem newMenuItem) 
	{
		super(newMenuItem);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// =================================================================================================
	// COMPARETO() returns -1, 1, or 0 depending on if the other MenuItem's Name is greater than, less
	// than, or equal to the NameComparator's MenuItem. It uses the String's built in compareTo method.
	// =================================================================================================
	public int compareTo(MenuItem anotherMenuItem) 
	{
		MenuItem myMenuItem = this.getMyObject();
		int Result;
		
		Result = myMenuItem.getName().compareTo(anotherMenuItem.getName());
		
		return Result;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////

	// =================================================================================================
	// TOSTRING() prints out object as a comma delimited string. Prints out what it compared.
	// =================================================================================================
	public String toString()
	{
		return "name";
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
}
