package MenuItemSystem;

// =====================================================================================================
// Meal.java
// -----------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-426: Advanced Programming
// Date Created: 09/12/13
// Last Modified: 12/01/13
// -----------------------------------------------------------------------------------------------------
// Meal is the child of MenuItem. Meal itself defines isSalad and cookTime. It can be constructed 
// normally, or by using the methods setData() and parseData(), which uses a comma delimited string to 
// parse and set its attributes.
// =====================================================================================================

public class Meal extends MenuItem
{
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//-------------------------------------------------------------------------------------------------
	// DECLARING attributes
	//-------------------------------------------------------------------------------------------------
	
	private boolean isSalad;
	private int cookTime;
	private boolean isOrganic;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// CONSTRUCTORS
	//--------------------------------------------------------------------------------------------------
	
	public Meal() 
	{
		super();
	}
	
	public Meal(String newName, float newCost, float newPrice, boolean newIsSalad, int newCookTime,
			boolean newIsOrganic) 
	{
		super(newName, newCost, newPrice);
		setIsSalad(newIsSalad);
		setCookTime(newCookTime);
		setIsOrganic(newIsOrganic);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// METHODS that parse a comma delimited string and sets attributes accordingly.
	//--------------------------------------------------------------------------------------------------
	
	// =================================================================================================
	// PARSEDATA() splits the string into an array, makes sure the right amount of attributes are 
	// present, then uses setDate() to set attributes.
	// =================================================================================================
	protected void parseData(String Data) throws NumberFormatException, IllegalArgumentException
	{
		String[] menuData = Data.split(",");
		
		if(menuData.length == 6)
		{
			super.setData(menuData);
			this.setData(menuData);
		}
		else
		{
			throw new IllegalArgumentException(myMenuException.getMsg(myMenuException.INVALID_FILE_FORMAT));
		}
	}
	
	// =================================================================================================
	// SETDATA() parses and sets members of the string array into the appropriate data type  for each 
	// attribute
	// =================================================================================================
	protected void setData(String[] menuData)
	{
		
		if(menuData[3].equalsIgnoreCase("T") || menuData[3].equalsIgnoreCase("True"))
		{
			setIsSalad(true);
		}
		else if(menuData[3].equalsIgnoreCase("F") || menuData[3].equalsIgnoreCase("False"))
		{
			setIsSalad(false);
		}
		else
		{
			throw new IllegalArgumentException(myMenuException.getMsg(myMenuException.INVALID_FILE_FORMAT));
		}
		
		setCookTime(Integer.parseInt(menuData[4]));
		
		if(menuData[5].equalsIgnoreCase("T") || menuData[5].equalsIgnoreCase("True"))
		{
			setIsOrganic(true);
		}
		else if(menuData[5].equalsIgnoreCase("F") || menuData[5].equalsIgnoreCase("False"))
		{
			setIsOrganic(false);
		}
		else
		{
			throw new IllegalArgumentException(myMenuException.getMsg(myMenuException.INVALID_FILE_FORMAT));
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// =================================================================================================
	// UNIQUESPECS() prints out the three unique specifications for each object
	// =================================================================================================
	public String uniqueSpecs()
	{
		String newLineMark = System.getProperty("line.separator");
		String leftAlignFormat = " %-7s | %-7s | %-7s |" + newLineMark;
		
		return String.format(leftAlignFormat, "" + isSalad, "" + cookTime, "" + isOrganic);
	}
	
	// =================================================================================================
	// PRINTTYPE() prints type of menuitem so we don't have to do an instanceof
	// =================================================================================================
	public String printType()
	{
		return "MEAL";
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// GETTERS and SETTERS for isSalad and cookTime
	//--------------------------------------------------------------------------------------------------

	protected boolean isSalad() 
	{
		return isSalad;
	}
	
	protected int getCookTime()
	{
		return cookTime;
	}
	
	protected boolean isOrganic()
	{
		return isOrganic;
	}

	protected void setIsSalad(boolean newIsSalad) 
	{
		isSalad = newIsSalad;
	}
	
	protected void setCookTime(int newCookTime)
	{
		cookTime = newCookTime;
	}
	
	protected void setIsOrganic(boolean newIsOrganic)
	{
		isOrganic = newIsOrganic;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
}
