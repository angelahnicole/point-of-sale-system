package MenuItemSystem;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;


// =====================================================================================================
// AppLogic.java
// -----------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-426: Advanced Programming
// Date Created: 09/12/13
// Last Modified: 12/01/13
// -----------------------------------------------------------------------------------------------------
// This class serves as a buffer between the user interface and data systems & file I/O, so the UI 
// doesn't directly interact with them. Also creates a simple interface for client to utilize.
// =====================================================================================================

public class AppLogic 
{
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// DECLARING attributes
	//--------------------------------------------------------------------------------------------------
	
	// Main attributes
	private ArrayList<MenuItem> myMenuList;
	private ArrayList<Order> myOrderList;
	private FoodStorage myFoodStorage;
	private OrderStorage myOrderStorage;
	private File myFile;
	private FileIO myFileIO;
	
	// Object factory
	private MenuItemFactory myMenuItemBuilder;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// CONSTRUCTORS
	//--------------------------------------------------------------------------------------------------
	
	public AppLogic() 
	{
		myFoodStorage = new FoodStorage();
		myOrderStorage = new OrderStorage();
		myMenuList = myFoodStorage.getMenuList();
		myOrderList = myOrderStorage.getOrderList();
		myFileIO = new FileIO(myFile, myFoodStorage);
		myMenuItemBuilder = new MenuItemFactory();
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////

	//--------------------------------------------------------------------------------------------------
	// METHODS that are for utility
	//--------------------------------------------------------------------------------------------------
	
	// =================================================================================================
	// READFILE() uses the FileIO method readFile() to read a file and add menu item objects to the list.
	// It also makes sure that each MenuItem has the correct currency object.
	// =================================================================================================
	public void readFile() throws FileNotFoundException, IllegalArgumentException
	{
		MenuItem myMenuItem;
		
		myFileIO.readFile();
		
		for(int i=0; i < myMenuList.size(); i++)
		{
			myMenuItem = myMenuList.get(i);
			myMenuItem.setCurrency(getCurrency());
		}
	}
	
	// =================================================================================================
	// CONVERTNUMBERS() takes apart the data string, converts it back from Currency format to unit 
	// format, and then reassemble the data string.
	// =================================================================================================
	public String convertNumbers(String Data)
	{
		String[] myData = Data.split(",");
		
		myData[2] = "" + (myFileIO.getCurrency()).ConvertBack(Float.parseFloat(myData[2]));
		myData[3] = "" + (myFileIO.getCurrency()).ConvertBack(Float.parseFloat(myData[3]));
		
		Data = "";
		
		for(int i = 0; i < myData.length; i++)
		{
			if(i != myData.length-1)
			{
				Data += myData[i] + ",";
			}
			else
			{
				Data += myData[i];
			}
		}
		
		return Data;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// METHODS that can create Orders and add MenuItems to an Order.
	//--------------------------------------------------------------------------------------------------
	
	// =================================================================================================
	// CREATEORDER() creates an order, sets the order's current currency, and then adds the order to 
	// OrderStorage, and returns the created order.
	// =================================================================================================
	public Order createOrder()
	{
		Order myOrder = new Order(getCurrency());
		myOrderStorage.addOrder(myOrder);
		
		return myOrder;
	}
	
	// =================================================================================================
	// ADDMENUITEMORDER() takes in the order index in the OrderStorage and the desired menu item, and 
	// adds that menu item to the Order at that index.
	// =================================================================================================
	public void addMenuItemOrder(int Index, MenuItem myMenuItem) throws IndexOutOfBoundsException
	{
		Order myOrder = myOrderStorage.findOrder(Index);
		myOrder.addItem(myMenuItem);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//-------------------------------------------------------------------------------------------------
	// METHODS that can add, delete, find, and edit menu items.
	//-------------------------------------------------------------------------------------------------
	
	// =================================================================================================
	// CREATEMENUITEM() parses the string into a MenuItem object using myMenuItemBuilder, and then uses
	// addItem() to add the MenuItem to FoodStorage's MenuList. First it must convert the numbers,
	// however.
	// =================================================================================================
	public void createMenuItem(String Data) throws NumberFormatException, IllegalArgumentException
	{
		Data = convertNumbers(Data);
		addMenuItem(myMenuItemBuilder.createMenuItem(Data));
	}
	
	// =================================================================================================
	// ADDMENUITEM() adds a MenuItem object to the ArrayList using the FoodStorage method addItem(), and
	// then sorts the array in ascending order.
	// =================================================================================================
	public void addMenuItem(MenuItem myMenuItem)
	{
		myFoodStorage.addItem(myMenuItem);
	}
	
	// =================================================================================================
	// FINDMENUITEM() uses the FoodStorage method findItem() to find the MenuItem and return its index.
	// =================================================================================================
	public int findMenuItem(String Name) throws IllegalArgumentException
	{
		return myFoodStorage.findItem(Name);
	}
	
	// =================================================================================================
	// DELETEMENUITEM() uses findMenuItem() to find the MenuItem (namely its index) and delete it.
	// =================================================================================================
	public void deleteMenuItem(int Index) throws IndexOutOfBoundsException
	{
		myFoodStorage.deleteItem(Index);
	}
	
	// =================================================================================================
	// EDITMENUITEM() uses the FoodStorage method editItem() to replace the MenuItem at the specified
	// index and re-sort the list.
	// =================================================================================================
	public void editMenuItem(int Index, String Data) throws IndexOutOfBoundsException
	{
		Data = convertNumbers(Data);
		MenuItem myMenuItem = myMenuItemBuilder.createMenuItem(Data);
		myFoodStorage.editItem(myMenuItem, Index);
	}
	
	// =================================================================================================
	// SORTMENUITEMS() uses the new comparator to sort the menu items
	// =================================================================================================
	public void sortMenuItems(Comparator<MenuItem> newComparator)
	{
		myFoodStorage.setComparator(newComparator);
		myFoodStorage.sortItems();
	}
	
	// =================================================================================================
	// SETMENUITEMCURRENCY() goes through FoodStorage and OrderStorage to reset the currency if it 
	// changes.
	// =================================================================================================
	public void setMenuItemCurrency(Currency newCurrency)
	{
		setCurrency(newCurrency);
		
		for(int i = 0; i < myMenuList.size(); i++)
		{
			(myMenuList.get(i)).setCurrency(newCurrency);
		}
		
		for(int i = 0; i < myOrderList.size(); i++)
		{
			(myOrderList.get(i)).setCurrency(newCurrency);
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// GETTERS and SETTERS for myFile, myFileIO, myMenuList, myCompator, myCurrency, and usingUserKey
	//--------------------------------------------------------------------------------------------------
	
	protected File getFile()
	{
		return myFile;
	}

	protected FileIO getFileIO()
	{
		return myFileIO;
	}

	protected ArrayList<MenuItem> getMenuList()
	{
		return myMenuList;
	}
	
	protected ArrayList<Order> getOrderList()
	{
		return myOrderList;
	}
	
	protected Comparator<MenuItem> getComparator()
	{
		return myFoodStorage.getComparator();
	}
	
	protected Currency getCurrency()
	{
		return myFileIO.getCurrency();
	}
	
	protected void setFile(File newFile)
	{
		myFile = newFile;
		myFileIO.setFile(newFile);
	}

	protected void setFileIO(FileIO newFileIO)
	{
		myFileIO = newFileIO;
	}

	protected void setMenuList(ArrayList<MenuItem> newMenuList)
	{
		myMenuList = newMenuList;
	}

	protected void setOrderList(ArrayList<Order> newOrderList)
	{
		myOrderList = newOrderList;
	}
	
	protected void setComparator(Comparator<MenuItem> myComparator)
	{
		myFoodStorage.setComparator(myComparator);
	}
	
	protected void setCurrency(Currency newCurrency)
	{
		myFileIO.setCurrency(newCurrency);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
}
