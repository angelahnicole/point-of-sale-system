package MenuItemSystem;
// =====================================================================================================
// IceCreamItem.java
// -----------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-426: Advanced Programming
// Date Created: 09/30/13
// Last Modified: 09/30/13
// -----------------------------------------------------------------------------------------------------
// IceCreamItem is the "outside" application to be used with an adapter class. IceCreamItem itself 
// defines Name, Cost, Price, Flavor, and IceCream. It can be constructed normally, or by using the
// methods setData() and parseData(), which uses a comma delimited string to parse and set its attributes.
// =====================================================================================================

public class IceCreamItem
{
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//-------------------------------------------------------------------------------------------------
	// DECLARING attributes
	//-------------------------------------------------------------------------------------------------
	
	private String Name;
	private float Cost;
	private float Price;
	private String Flavor;
	private String iceCream;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// CONSTRUCTORS
	//--------------------------------------------------------------------------------------------------
	
	public IceCreamItem(){}
	
	public IceCreamItem(String newName, float newCost, float newPrice, String newFlavor, String newIceCream) 
	{
		setName(newName);
		setCost(newCost);
		setPrice(newPrice);
		setFlavor(newFlavor);
		setIceCream(newIceCream);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// METHODS that parse a comma delimited string and sets attributes accordingly.
	//--------------------------------------------------------------------------------------------------
	
	// =================================================================================================
	// PARSEDATA() splits the string into an array, makes sure the right amount of attributes are 
	// present, then uses setDate() to set attributes.
	// =================================================================================================
	protected void parseData(String Data) throws NumberFormatException, IllegalArgumentException
	{
		String[] menuData = Data.split(",");
		
		if(menuData.length == 5)
		{
			this.setData(menuData);
		}
		else
		{
			throw new IllegalArgumentException("****ERROR: Input is invalid. Please try again.****");
		}
	}
	
	// =================================================================================================
	// SETDATA() parses and sets members of the string array into the appropriate data type  for each 
	// attribute
	// =================================================================================================
	protected void setData(String[] menuData) throws NumberFormatException, IllegalArgumentException
	{
		if(menuData[0].length() <= 30)
		{
			setName(menuData[0]);
			setCost(Float.parseFloat(menuData[1]));
			setPrice(Float.parseFloat(menuData[2]));
			setFlavor(menuData[3]);
			setIceCream(menuData[4]);
		}
		else
		{
			throw new IllegalArgumentException("****ERROR: Input is invalid. Please try again.****");
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// =================================================================================================
	// TOSTRING() prints out object as a tabbed string
	// =================================================================================================
	public String toString()
	{
		return Name + ", \t" + Cost + ", \t" + Price + ", \t" + Flavor + ", \t" + iceCream;
	}
	
	// =================================================================================================
	// UNIQUESPECS() prints out the three unique specifications for each object
	// =================================================================================================
	public String uniqueSpecs()
	{
		return "";
	}
	
	// =================================================================================================
	// PRINTTYPE() prints type of menuitem so we don't have to do an instanceof
	// =================================================================================================
	public String printType()
	{
		return "ICECREAMITEM";
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	//--------------------------------------------------------------------------------------------------
	// GETTERS and SETTERS for Name, Cost, Price, Flavor, and IceCream.
	//--------------------------------------------------------------------------------------------------

	protected String getName()
	{
		return Name;
	}

	protected float getCost()
	{
		return Cost;
	}

	protected float getPrice()
	{
		return Price;
	}

	protected String getFlavor()
	{
		return Flavor;
	}
	
	protected String getIceCream()
	{
		return iceCream;
	}

	protected void setName(String newName)
	{
		Name = newName;
	}

	protected void setCost(float newCost)
	{
		Cost = newCost;
	}

	protected void setPrice(float newPrice)
	{
		Price = newPrice;
	}

	protected void setFlavor(String newFlavor)
	{
		Flavor = newFlavor;
	}
	
	protected void setIceCream(String newIceCream)
	{
		iceCream = newIceCream;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
}
