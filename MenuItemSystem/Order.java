package MenuItemSystem;

import java.text.DecimalFormat;
import java.util.ArrayList;

// =====================================================================================================
// Order.java
// -----------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-426: Advanced Programming
// Date Created: 11/10/13
// Last Modified: 12/01/13
// -----------------------------------------------------------------------------------------------------
// Generates orders of MenuItems by keeping track of number, type, and current currency of menuItems 
// along with its orderNumber. It also has a varying amount of states, depending on what the user
// wants to do with the Order (adding, paying, tending, etc.). Uses the OrderState hierarchy in order
// to accomplish this.
// =====================================================================================================

public class Order 
{
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// DECLARING attributes
	//--------------------------------------------------------------------------------------------------
	
	// Order States
	private OrderState newState;
	private OrderState inProcessState;
	private OrderState tendedState;
	private OrderState paidState;
	private OrderState currentState;
	
	// Main Attributes
	private ArrayList<MenuItem> myMenuItems;
	private ArrayList<Integer> numMenuItems;
	private Currency myCurrency;
	private int orderNumber;
	private float totalPrice; // UNIT price (currency)
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// CONSTRUCTOR
	//--------------------------------------------------------------------------------------------------
	
	public Order(Currency newCurrency)
	{
		myCurrency = newCurrency;
		
		// create states
		newState = new New(this);
		inProcessState = new InProcess(this);
		tendedState = new Tended(this);
		paidState = new Paid(this);
		
		// set state and create order
		setCurrentState(newState);
		currentState.createOrder();
		
		// create menu list
		myMenuItems = new ArrayList<MenuItem>();
		numMenuItems = new ArrayList<Integer>();
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// METHODS that find and add MenuItems to order
	//--------------------------------------------------------------------------------------------------
	
	// =================================================================================================
	// ADDITEM() adds MenuItem to end of order if not found and sets its corresponding numMenuItems index
	// to 1. If found, it increments its corresponding numMenuItems index by 1.
	// =================================================================================================
	public void addItem(MenuItem myMenuItem)
	{	
		if(isItemInList(myMenuItem.getName()) == false)
		{
			myMenuItems.add(myMenuItem);
			numMenuItems.add(1);
		}
		
		currentState.addItem();
	}
	
	// =================================================================================================
	// ISITEMINLIST() searches myMenuItems for that particular item. If it finds it, it increments
	// its corresponding numMenuItems index by 1.
	// =================================================================================================
	public boolean isItemInList(String menuItemName)
	{
		for(int i = 0; i < myMenuItems.size(); i++)
		{
			if((myMenuItems.get(i).getName()).equalsIgnoreCase(menuItemName))
			{
				numMenuItems.set(i, numMenuItems.get(i) + 1);
				return true;
			}
		}
		return false;
	}
	
	// =================================================================================================
	// TENDORDER sets this order's state to tended
	// =================================================================================================
	public void tendOrder()
	{
		currentState.giveOrder();
	}
	
	// =================================================================================================
	// PAYORDER() sets this order's state to paid
	// =================================================================================================
	public void payOrder()
	{
		currentState.payOrder();
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// =================================================================================================
	// CREATECURRENCYFORMAT() returns a DecimalFormat object according to the current Currency object.
	// =================================================================================================
	public DecimalFormat createCurrencyFormat()
	{
		DecimalFormat myCurrencyFormat;
		
		if(myCurrency != null)
		{
			myCurrencyFormat = new DecimalFormat(myCurrency + "###,###.##");
		}
		else
		{
			myCurrencyFormat = new DecimalFormat("$###,###.##");
			System.out.println("ERROR: Currency is null, but has defaulted to USD. These conversions may be incorrect.");
			System.out.println();
		}
		
		myCurrencyFormat.setMaximumFractionDigits(2);
		myCurrencyFormat.setMinimumFractionDigits(2);
		
		return myCurrencyFormat;
	}
	
	// =================================================================================================
	// PRINTTOTAL() takes in a decimal format to correctly print out the order total if the current 
	// state is Paid or Tended. If not, then it just prints out N/A.
	// =================================================================================================
	public String printTotal()
	{
		String Total = "N/A";
		DecimalFormat myCurrencyFormat;
		
		if(currentState instanceof Paid || currentState instanceof Tended)
		{
			// only need to create this if we're using it
			myCurrencyFormat = createCurrencyFormat();
			
			Total = myCurrencyFormat.format(getTotalPrice());
		}
		
		return Total;
	}
	
	// =================================================================================================
	// TOSTRING() prints out order details, which is the order number, state, and the total
	// =================================================================================================
	public String toString()
	{
		String newLineMark = System.getProperty("line.separator");
		String orderAlignFormat = "| %-14s | %-10s | %-10s |" + newLineMark;
		return String.format(orderAlignFormat, "" + orderNumber, currentState, printTotal());
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// GETTERS and SETTERS for Currency, States, orderNumber, totalPrice, myMenuItems, and numMenuItems
	//--------------------------------------------------------------------------------------------------
	
	protected Currency getCurrency()
	{
		return myCurrency;
	}
	
	protected OrderState getCurrentState()
	{
		return currentState;
	}
	
	protected OrderState getNewState()
	{
		return newState;
	}
	
	protected OrderState getInProcessState()
	{
		return inProcessState;
	}
	
	protected OrderState getTendedState()
	{
		return tendedState;
	}
	
	protected OrderState getPaidState()
	{
		return paidState;
	}
	
	protected int getOrderNumber() 
	{
		return orderNumber;
	}
	
	protected float getTotalPrice()
	{
		totalPrice = 0f;
		for(int i = 0; i < myMenuItems.size(); i++)
		{
			totalPrice += (numMenuItems.get(i)) * myMenuItems.get(i).getPrice();
		}
		
		return totalPrice;
	}
	
	protected ArrayList<MenuItem> getMyMenuItems() 
	{
		return myMenuItems;
	}

	protected ArrayList<Integer> getNumMenuItems() 
	{
		return numMenuItems;
	}
	
	protected void setCurrency(Currency newCurrency)
	{
		myCurrency = newCurrency;
	}
	
	protected void setCurrentState(OrderState newState)
	{
		currentState = newState;
	}

	protected void setOrderNumber(int newOrderNumber) 
	{
		orderNumber = newOrderNumber;
	}
	
	protected void setTotalPrice(float newTotalPrice)
	{
		totalPrice = newTotalPrice;
	}
	
	protected void setMyMenuItems(ArrayList<MenuItem> newMenuItems) 
	{
		myMenuItems = newMenuItems;
	}
	
	protected void setNumMenuItems(ArrayList<Integer> newNumMenuItems) 
	{
		numMenuItems = newNumMenuItems;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
}
