package MenuItemSystem;

// =====================================================================================================
// ComparatorFactory.java
//------------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-426: Advanced Programming
// Date Created: 09/30/13
// Last Modified: 10/01/13
// -----------------------------------------------------------------------------------------------------
// MenuItemFactory is a factory class that creates MenuItem objects.
// =====================================================================================================

public class ComparatorFactory
{
	////////////////////////////////////////////////////////////////////////////////////////////////////

	//--------------------------------------------------------------------------------------------------
	// DECLARING attribute
	//--------------------------------------------------------------------------------------------------
	
	// Provides exception messages by code
	private MenuExceptionCode myMenuException;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// CONSTRUCTOR
	//--------------------------------------------------------------------------------------------------
	
	public ComparatorFactory()
	{
		myMenuException = new MenuExceptionCode();
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// =============================================================================================
	// CREATECOMPARATOR() creates a comparator object and returns it.
	// =============================================================================================
	public Comparator<MenuItem> createComparator(String Data) throws IllegalArgumentException
	{
		Comparator<MenuItem> myComparator;
		
		if(Data.equalsIgnoreCase("COST"))
		{
			myComparator = new CostComparator();
		}
		else if(Data.equalsIgnoreCase("PRICE"))
		{
			myComparator = new PriceComparator();
		}
		else if(Data.equalsIgnoreCase("PROFIT"))
		{
			myComparator = new ProfitComparator();
		}
		else if(Data.equalsIgnoreCase("NAME"))
		{
			myComparator = new NameComparator();
		}
		else
		{
			throw new IllegalArgumentException(myMenuException.getMsg(myMenuException.INVALID_INPUT)); 
		}
		
		return myComparator;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
}
