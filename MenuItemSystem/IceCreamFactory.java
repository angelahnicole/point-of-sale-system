package MenuItemSystem;


// =====================================================================================================
// IceCreamFactory.java
//------------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-426: Advanced Programming
// Date Created: 09/30/13
// Last Modified: 10/01/13
// -----------------------------------------------------------------------------------------------------
// IceCreamFactory is a factory class that creates IceCreamItem objects. IceCream, an adapter class,
// helps interface between MenuItem and Soda & Milkshake via an IceCreamItem object.
// =====================================================================================================

public class IceCreamFactory
{
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// DECLARING attribute
	//--------------------------------------------------------------------------------------------------
	
	// Provides exception messages by code
	private MenuExceptionCode myMenuException;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// CONSTRUCTOR
	//--------------------------------------------------------------------------------------------------
	
	public IceCreamFactory()
	{
		myMenuException = new MenuExceptionCode();
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// =============================================================================================
	// CREATEICECREAM() creates and returns an IceCreamItem, making use of each IceCreamItem object's 
	// parseData() method, which takes in a string and populates each item accordingly.
	// =============================================================================================
	public IceCreamItem createIceCream(String Data) throws NumberFormatException, IllegalArgumentException
	{
		String[] iceCreamData = Data.split(",");
		IceCreamItem myIceCreamItem;
		
		if(iceCreamData[0].equalsIgnoreCase("SODA"))
		{
			Data = Data.substring(5);
			myIceCreamItem = new Soda();
			myIceCreamItem.parseData(Data);
		}
		else if(iceCreamData[0].equalsIgnoreCase("MILKSHAKE"))
		{
			Data = Data.substring(10);
			myIceCreamItem = new Milkshake();
			myIceCreamItem.parseData(Data);
		}
		else
		{
			throw new IllegalArgumentException(myMenuException.getMsg(myMenuException.INVALID_INPUT)); 
		}
		
		return myIceCreamItem;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
}
