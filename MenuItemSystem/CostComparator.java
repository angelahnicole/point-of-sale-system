package MenuItemSystem;

// =====================================================================================================
// CostComparator.java
// -----------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-426: Advanced Programming
// Date Created: 09/12/13
// Last Modified: 09/30/13
// -----------------------------------------------------------------------------------------------------
// CostComparator is a child of Comparator. It uses the parent class, which takes in an object to 
// compare others to (This child uses MenuItem objects). Its primary use is to compare MenuItem object's
// costs.
// =====================================================================================================

public class CostComparator extends Comparator<MenuItem>
{
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// CONSTRUCTORS
	//--------------------------------------------------------------------------------------------------
	
	public CostComparator() {}
	
	public CostComparator(MenuItem newMenuItem) 
	{
		super(newMenuItem);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// =================================================================================================
	// COMPARETO() returns -1, 1, or 0 depending on if the other MenuItem's cost is greater than, less
	// than, or equal to the CostComparator's MenuItem.
	// =================================================================================================
	public int compareTo(MenuItem anotherMenuItem) 
	{
		MenuItem myMenuItem = super.getMyObject();
		int Result;
			
		if(myMenuItem.getCost() > anotherMenuItem.getCost())
		{
			Result = 1;
		}
		else if(myMenuItem.getCost() < anotherMenuItem.getCost())
		{
			Result = -1;
		}
		else
		{
			Result = 0;
		}
		
		return Result;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////

	// =================================================================================================
	// TOSTRING() prints out object as a comma delimited string. Prints out what it compared.
	// =================================================================================================
	public String toString()
	{
		return "cost";
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
}
