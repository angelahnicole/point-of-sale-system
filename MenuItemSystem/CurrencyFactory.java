package MenuItemSystem;

// =====================================================================================================
// CurrencyFactory.java
//------------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-426: Advanced Programming
// Date Created: 11/30/13
// Last Modified: 11/30/13
// -----------------------------------------------------------------------------------------------------
// CurrencyFactory is a factory class that creates Currency objects.
// =====================================================================================================

public class CurrencyFactory
{
	////////////////////////////////////////////////////////////////////////////////////////////////////

	//--------------------------------------------------------------------------------------------------
	// DECLARING attribute
	//--------------------------------------------------------------------------------------------------
	
	// Provides exception messages by code
	private MenuExceptionCode myMenuException;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// CONSTRUCTOR
	//--------------------------------------------------------------------------------------------------
	
	public CurrencyFactory()
	{
		myMenuException = new MenuExceptionCode();
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// =============================================================================================
	// CREATECURRENCY()
	// =============================================================================================
	public Currency createCurrency(String Data) throws NumberFormatException, IllegalArgumentException
	{
		Currency myCurrency = null;
		
		if(Data.equalsIgnoreCase("DOLLAR"))
		{
			myCurrency = new Dollars();
		}
		else if(Data.equalsIgnoreCase("YEN"))
		{
			myCurrency = new Yens();
		}
		else if(Data.equalsIgnoreCase("EURO"))
		{
			myCurrency = new Euros();
		}
		else
		{
			throw new IllegalArgumentException(myMenuException.getMsg(myMenuException.INVALID_INPUT)); 
		}
		
		return myCurrency;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
}
