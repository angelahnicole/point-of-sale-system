package MenuItemSystem;

// =====================================================================================================
// Milkshake.java
// -----------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-426: Advanced Programming
// Date Created: 09/30/13
// Last Modified: 12/01/13
// -----------------------------------------------------------------------------------------------------
// Milkshake is the child of IceCreamItem. Milkshake itself only defines maltPowderScoops. It can be 
// constructed normally, or by using the methods setData() and parseData(), which uses a comma 
// delimited string to parse and set its attributes.
// =====================================================================================================

public class Milkshake extends IceCreamItem
{
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//-------------------------------------------------------------------------------------------------
	// DECLARING attributes
	//-------------------------------------------------------------------------------------------------
	
	private int numMaltScoops;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// CONSTRUCTORS
	//--------------------------------------------------------------------------------------------------
	
	public Milkshake()
	{
		super();
	}
	
	public Milkshake(String newName, float newCost, float newPrice, String newFlavor, String newIceCream,
			int newNumMaltScoops)
	{
		super(newName, newCost, newPrice, newFlavor, newIceCream);
		setNumMaltScoops(newNumMaltScoops);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// METHODS that parse a comma delimited string and sets attributes accordingly.
	//--------------------------------------------------------------------------------------------------
	
	// =================================================================================================
	// PARSEDATA() splits the string into an array, makes sure the right amount of attributes are 
	// present, then uses setDate() to set attributes.
	// =================================================================================================
	protected void parseData(String Data) throws NumberFormatException, IllegalArgumentException
	{
		String[] menuData = Data.split(",");
		
		if(menuData.length == 6)
		{
			super.setData(menuData);
			this.setData(menuData);
		}
		else
		{
			throw new IllegalArgumentException("****ERROR: Input is invalid. Please try again.****");
		}
	}
	
	// =================================================================================================
	// SETDATA() parses and sets members of the string array into the appropriate data type  for each 
	// attribute
	// =================================================================================================
	protected void setData(String[] menuData) throws NumberFormatException
	{
		setNumMaltScoops(Integer.parseInt(menuData[5]));
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// =================================================================================================
	// UNIQUESPECS() prints out the three unique specifications for each object
	// =================================================================================================
	public String uniqueSpecs()
	{
		String newLineMark = System.getProperty("line.separator");
		String leftAlignFormat = " %-7s | %-7s | %-7s |" + newLineMark;
		
		return String.format(leftAlignFormat, getFlavor(), getIceCream(), "" + numMaltScoops);
	}
	
	// =================================================================================================
	// PRINTTYPE() prints type of menuitem so we don't have to do an instanceof
	// =================================================================================================
	public String printType()
	{
		return "MILKSHAKE";
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// GETTERS and SETTERS
	//--------------------------------------------------------------------------------------------------

	protected int getNumMaltScoops()
	{
		return numMaltScoops;
	}

	protected void setNumMaltScoops(int newNumMaltScoops)
	{
		numMaltScoops = newNumMaltScoops;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
}
