package MenuItemSystem;

import javax.swing.JOptionPane;

// =====================================================================================================
// Tended.java
// -----------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-426: Advanced Programming
// Date Created: 11/18/13
// Last Modified: 12/01/13
// -----------------------------------------------------------------------------------------------------
//	Tended is a child of OrderState, which means that it's a State that implements its parent's 
//	State Transitions. It has an instance of Order, because an order can be in a multitude of states.
//	It changes the Order's state according to the State Transition called, and it notifies the user
//	of the change/if the change cannot be completed.
// =====================================================================================================

public class Tended implements OrderState 
{
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// -------------------------------------------------------------------------------------------------
	// DECLARING attribute
	// -------------------------------------------------------------------------------------------------
	
	// Main attribute
	private Order myOrder;
	
	// MenuExceptionCode object that retrieves error messages
	private MenuExceptionCode myMenuException;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// -------------------------------------------------------------------------------------------------
	// CONSTRUCTOR
	// -------------------------------------------------------------------------------------------------
	
	public Tended(Order newOrder)
	{
		setOrder(newOrder);
		myMenuException = new MenuExceptionCode();
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// -------------------------------------------------------------------------------------------------
	// STATE TRANSITIONS that children states implement for orders
	// -------------------------------------------------------------------------------------------------
	
	// =================================================================================================
	// CREATEORDER() is sort of useless, but it's mostly there as a trivial base case.
	// =================================================================================================
	public void createOrder() 
	{
		// Notify User
		JOptionPane.showMessageDialog(null, myMenuException.getMsg(myMenuException.ORDER_STATE_ALREADY_CREATED), "Error", JOptionPane.ERROR_MESSAGE);
		return;
	}

	// =================================================================================================
	// ADDITEM() is used to set unpaid orders to In Process. Otherwise, error.
	// =================================================================================================
	public void addItem() 
	{
		// Set state
		myOrder.setCurrentState(myOrder.getInProcessState());
		
		// Notify User
		JOptionPane.showMessageDialog(null, "Menu Item successfully added!");
		return;
	}

	// =================================================================================================
	// GIVEORDER() is used to show the customer the order total when it's In Process. Otherwise, error.
	// =================================================================================================
	public void giveOrder()
	{
		// Notify User
		JOptionPane.showMessageDialog(null, myMenuException.getMsg(myMenuException.ORDER_STATE_ALREADY_IN), "Error", JOptionPane.ERROR_MESSAGE);
		return;
	}

	// =================================================================================================
	// PAYORDER() is used for the customer to pay the order when it's In Process or Tended. Otherwise, 
	// error.
	// =================================================================================================
	public void payOrder() 
	{
		// Set state
		myOrder.setCurrentState(myOrder.getPaidState());
		
		// Notify User
		JOptionPane.showMessageDialog(null, "Order Successfully Paid!");
		return;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ------------------------------------------------------------------------------------------------
	// TOSTRING method 
	// ------------------------------------------------------------------------------------------------
	
	// =================================================================================================
	// TOSTRING() let's user know which state the order is in
	// =================================================================================================
	public String toString()
	{
		return "Tended";
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// -------------------------------------------------------------------------------------------------
	// GETTER and SETTER for Order
	// -------------------------------------------------------------------------------------------------
	
	protected Order getOrder()
	{
		return myOrder;
	}
	
	protected void setOrder(Order newOrder)
	{
		myOrder = newOrder;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
}
