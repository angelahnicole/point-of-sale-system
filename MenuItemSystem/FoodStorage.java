package MenuItemSystem;

import java.util.ArrayList;

// =====================================================================================================
// FoodStorage.java
// -----------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-426: Advanced Programming
// Date Created: 09/12/13
// Last Modified: 11/30/13
// -----------------------------------------------------------------------------------------------------
// FoodStorage maintains an ArrayList that is sorted by using heap sort. It can add, delete, sort, and 
// find MenuItems in this list. It uses basic MenuItem and Comparator classes in order to support
// polymorphic overrides of child Comparator and MenuItem objects.
// =====================================================================================================

public class FoodStorage 
{
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// DECLARING attributes
	//--------------------------------------------------------------------------------------------------
	
	// Main attributes
	private ArrayList<MenuItem> myMenuList;
	private Comparator<MenuItem> myComparator;
	private MenuHeap myMenuHeap;
	
	// Provides exception messages by code
	private MenuExceptionCode myMenuException;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// CONSTRUCTORS
	//--------------------------------------------------------------------------------------------------
	
	public FoodStorage()
	{
		myMenuList = new ArrayList<MenuItem>();
		myMenuHeap = new MenuHeap(myMenuList, myComparator);
		myComparator = null;
		myMenuException = new MenuExceptionCode();
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// METHODS that add, delete, sort, and find MenuItem objects.
	//--------------------------------------------------------------------------------------------------
	
	// =================================================================================================
	// ADDITEM() adds a MenuItem object to the ArrayList, and then sorts the array in ascending order.
	// =================================================================================================
	public void addItem(MenuItem myMenuItem)
	{
		myMenuList.add(myMenuItem);
		
		sortItems();
	}
	
	// =================================================================================================
	// FINDITEM() searches through the names of MenuItem objects in the ArrayList- if it finds a match,
	// it returns the index of the object.
	// =================================================================================================
	public int findItem(String Name) throws IllegalArgumentException
	{
		MenuItem myMenuItem;
		int resultIndex = -1;
		
		for(int i=0; i < myMenuList.size(); i++)
		{
			myMenuItem = myMenuList.get(i);
			
			if((myMenuItem.getName()).equalsIgnoreCase(Name))
			{
				resultIndex = i;
				break;
			}
		}
		if(resultIndex == -1)
		{
			throw new IllegalArgumentException(myMenuException.getMsg(myMenuException.MENU_ITEM_NOT_FOUND));
		}
		
		return resultIndex;
	}
	
	// =================================================================================================
	// DELETEITEM() removes a MenuItem object from the ArrayList, and then sorts the array once more.
	// =================================================================================================
	public void deleteItem(int i) throws IndexOutOfBoundsException
	{	
		if(myMenuList.isEmpty())
		{
			throw new IndexOutOfBoundsException(myMenuException.getMsg(myMenuException.MENU_LIST_EMPTY));
		}
		
		if(i >= myMenuList.size() || i < 0)
		{
			throw new IndexOutOfBoundsException(myMenuException.getMsg(myMenuException.MENU_ITEM_NOT_FOUND));
		}
		
		myMenuList.remove(i);
		
		sortItems();
	}
	
	// =================================================================================================
	// EDITITEM() takes in a new MenuItem object and replaces the old MenuItem object at index i, and
	// then resorts the items just in case the edit fussed up the order.
	// =================================================================================================
	public void editItem(MenuItem myMenuItem, int i) throws IndexOutOfBoundsException
	{	
		if(myMenuList.isEmpty())
		{
			throw new IndexOutOfBoundsException(myMenuException.getMsg(myMenuException.MENU_LIST_EMPTY));
		}
		
		if(i >= myMenuList.size() || i < 0)
		{
			throw new IndexOutOfBoundsException(myMenuException.getMsg(myMenuException.MENU_ITEM_NOT_FOUND));
		}
			
		myMenuList.set(i, myMenuItem);
		sortItems();
	}
	
	// =================================================================================================
	// SORTITEMS() utilizes MenuHeap to sort the MenuItems in ascending order, depending on the
	// Comparator object specified. 
	// =================================================================================================
	public void sortItems()
	{
		setMenuList(myMenuHeap.sortHeap());
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------------------------------------------------
	// GETTERS and SETTERS for myMenuList and Comparator
	//--------------------------------------------------------------------------------------------------
	
	protected ArrayList<MenuItem> getMenuList()
	{
		return myMenuList;
	}
	
	protected Comparator<MenuItem> getComparator()
	{
		return myComparator;
	}
	
	protected void setComparator(Comparator<MenuItem> newComparator)
	{
		myComparator = newComparator;
		myMenuHeap.setHeapComparator(newComparator);
	}
	
	protected void setMenuList(ArrayList<MenuItem> newMenuList)
	{
		myMenuList = newMenuList;
		myMenuHeap.setHeapMenuList(newMenuList);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// =================================================================================================
	// MenuHeap Data Structure
	// -------------------------------------------------------------------------------------------------
	// Utilizes heap sort with an ArrayList.
	// =================================================================================================
	private class MenuHeap
	{
		////////////////////////////////////////////////////////////////////////////////////////////////
		
		//----------------------------------------------------------------------------------------------
		// DECLARING attributes
		//----------------------------------------------------------------------------------------------
		
		private ArrayList<MenuItem> myMenuList;
		private Comparator<MenuItem> myComparator;
		int N;
		
		////////////////////////////////////////////////////////////////////////////////////////////////
		
		//----------------------------------------------------------------------------------------------
		// CONSTRUCTOR
		//----------------------------------------------------------------------------------------------
		
		public MenuHeap(ArrayList<MenuItem> newMenuList, Comparator<MenuItem> newComparator)
		{
			myMenuList = newMenuList;
			myComparator = newComparator;
			N = 0;
		}
		
		////////////////////////////////////////////////////////////////////////////////////////////////
		
		//----------------------------------------------------------------------------------------------
		// METHODS that implement heap sort in order to sort MenuItems.
		//----------------------------------------------------------------------------------------------
		
		// =============================================================================================
		// HEAPIFY() creates a heap structure out of the ArrayList, starting with the last item that
		// leaves as its children.
		// =============================================================================================
		public void Heapify()
		{
			N = myMenuList.size()-1;
			
			for(int i = (N/2); i >= 0; i--)
			{
				maxHeapify(i);
			}
		}

		// =============================================================================================
		// MAXHEAPIFY() creates a max heap by making sure the parent is the largest of its children.
		// =============================================================================================
		public void maxHeapify(int i)
		{	
			MenuItem maxMenuItem, rightMenuItem, leftMenuItem;
			
			int Left = 2*i;
			int Right = (2*i) + 1;
			int Max = i;
			
			maxMenuItem = myMenuList.get(Max);
			myComparator.setMyObject(maxMenuItem);
			
			if(Left <= N)
			{
				leftMenuItem = myMenuList.get(Left);
				
				if(myComparator.compareTo(leftMenuItem) < 0)
				{
					Max = Left;
				}
			}
			
			maxMenuItem = myMenuList.get(Max);
			myComparator.setMyObject(maxMenuItem);
			
			if(Right <= N)
			{
				rightMenuItem = myMenuList.get(Right);
				
				if(myComparator.compareTo(rightMenuItem) < 0)
				{
					Max = Right;
				}
			}	
			
			if(Max != i)
			{
				swapItems(i, Max);
				maxHeapify(Max);
			}
		}
		
		// =============================================================================================
		// SORTHEAP() sorts the ArrayList by "taking off" (or moving to the end) the root of the heap,
		// as that is largest item in the heap. It then replaces the root of the heap with the last item,
		// or one of the smallest items in the heap. It decreases N by one (we "took off" the max), and
		// then rebalances the max heap. By the end of this, we'll end up with a sorted ArrayList.
		// =============================================================================================
		public ArrayList<MenuItem> sortHeap()
		{	
			Heapify();
			
			for(int i = N; i > 0; i--)
			{	
				int maxItem = i;
				int firstItem = 0;
				
				swapItems(firstItem, maxItem);
				N = N-1;
				maxHeapify(firstItem);
			}
			
			return myMenuList;
		}
		
		// =============================================================================================
		// SWAPITEMS() swaps ArrayList entries with indicies i and j.
		// =============================================================================================
		public void swapItems(int i, int j)
		{
			MenuItem tempMenuItem = myMenuList.get(i);
			myMenuList.set(i, myMenuList.get(j));
			myMenuList.set(j, tempMenuItem);
		}
		
		////////////////////////////////////////////////////////////////////////////////////////////////
		
		//----------------------------------------------------------------------------------------------
		// GETTERS and SETTERS for myMenuList and Comparator
		//----------------------------------------------------------------------------------------------

		protected ArrayList<MenuItem> getHeapMenuList()
		{
			return myMenuList;
		}
		
		protected Comparator<MenuItem> getHeapComparator()
		{
			return myComparator;
		}
		
		protected void setHeapMenuList(ArrayList<MenuItem> newMenuList)
		{
			myMenuList = newMenuList;
		}
		
		protected void setHeapComparator(Comparator<MenuItem> newComparator)
		{
			myComparator = newComparator;
		}
		
		////////////////////////////////////////////////////////////////////////////////////////////////
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////
}
