# README #

A point of sale system that was created for my advanced programming class that made use of a great deal of design patterns.

## Some screenshots... ##

![POS_Screen-1.png](https://bitbucket.org/repo/8n5pRa/images/4127445710-POS_Screen-1.png)

![POS_Screen-2.png](https://bitbucket.org/repo/8n5pRa/images/2141059538-POS_Screen-2.png)